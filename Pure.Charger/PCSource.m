//
//  PCSource.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 13/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCSource.h"

#pragma mark - Titles
NSString* const TITLEBARCREATEACCOUNT = @"Create account";
NSString* const TITLEBARUSERPOINTS = @"My points";
NSString* const TITLEBARUSERHISTORY = @"History";
NSString* const TITLEBARSTORE = @"Store";
NSString* const TITLEBARSETTINGS = @"Settings";
NSString* const TITLEBARABOUTAPP = @"About App";
NSString* const TITLEBARACHIEVEMENTS = @"My Achievements";

#pragma mark - Keys
NSString* const USERNAMEKEY = @"UserName";
NSString* const USERPASSWORDKEY = @"UserPassword";
NSString* const USEREMAILKEY = @"UserEmail";
NSString* const USERPICTUREPATHKEY = @"UserPicturePathString";

#pragma mark - Alerts
NSString* const TITLEALERTERROR = @"Error";
NSString* const TITLEALERTWRONGNAME = @"Error";
NSString* const TITLEALERTWRONGEMAIL = @"Error";
NSString* const TITLEALERTWRONGPASSWORD = @"Error";

NSString* const TEXTALERTWRONGNAME = @"Wrong name";
NSString* const TEXTALERTWRONGPASSWORD = @"Wrong password";
NSString* const TEXTALERTWRONGDIGITS = @"Only Latin letters are allowed";
NSString* const TEXTALERTWRONGEMAIL = @"Incorrect email";
NSString* const TEXTALERTWRONGPASSWORDLENGTH = @"Password must be at least 8 symbols length";
NSString* const TEXTALERTWRONGDATA = @"Incorrect data";

NSString* const TEXTALERTDATASAVED = @"Account is saved";

NSString* const TEXTBONUSLABEL = @"Last achievement bonus";

#pragma mark - Text
NSString* const TEXTCONNECTED = @"Connected";

#pragma mark - Sizes
NSInteger const SIDEBARWIDTH = 300;
NSInteger const USERPASSWORDLENGTH = 8;
NSInteger const HISTORYCELLHEIGHT = 60;
NSInteger const SMALLCELLHEIGHT = 44;
NSInteger const USERPOINTSCELLHEIGHT = 150;
NSInteger const USERPOINTSBIGCELLHEIGHT = 220;
NSInteger const STORECELLHEIGHT = 335;
NSInteger const TABLEHEADERHEIGHT = 130;
NSInteger const SECTIONHEADERHEIGHT = 20;
NSInteger const TEXTFIELDBORDER = 2;

NSInteger const SMALLPOPUPWIDTH = 270; //240;
NSInteger const SMALLPOPUPHEIGHT = 250; //120;
NSInteger const MEDIUMPOPUPWIDTH = 290; //240;
NSInteger const MEDIUMPOPUPHEIGHT = 275; //120;
NSInteger const BIGPOPUPWIDTH = 300;
NSInteger const BIGPOPUPHEIGHT = 370;
NSInteger const SQUAREPOPUPWIDTH = 287;
NSInteger const SQUAREPOPUPHEIGHT = 285;
NSInteger const CREATEACCOUNTBUTTONHEIGHT = 68;
NSInteger const ADDPICTUREBUTTONWIDTH = 60;
NSInteger const TRADEMARKLABELHEIGHT = 20;
NSInteger const TITLELABELHEIGHT = 35;

NSInteger const ITEMSOFFSET = 10;
NSInteger const CIRCLERADIUS = 145;
NSInteger const ARCWIDTH = 10;
NSInteger const CIRCLEANIMATIONDURATION = 3;
NSInteger const HISTORYBUTTONSOFFSET = 16;
NSInteger const HISTORYMAPBUTTONOFFSET = 44;

NSInteger const HISTORYCONTAINERHEIGHT = 70;
NSInteger const VISIBLEHISTORYCONTAINERHEIGHT = 80;
NSInteger const MINMAPHEIGHT = 200;