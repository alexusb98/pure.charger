//
//  PCDeviceListView.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 10/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCDeviceListView : UIView

@property (weak, nonatomic) IBOutlet UIButton *chargerButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UILabel *chargerLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;



+ (PCDeviceListView *) loadDeviceListView;

@end
