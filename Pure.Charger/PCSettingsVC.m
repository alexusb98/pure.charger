//
//  PCSettingsVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCSettingsVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCSettingsStandartCell.h"
#import "PCSettingsDetailCell.h"
#import "PCSettingsSwitchCell.h"
#import "PCSettingsHeaderCell.h"

@interface PCSettingsVC () {
    NSArray *imageSource0;
    NSArray *imageSource1;

    NSArray *dataSource0;
    NSArray *dataSource1;
    NSArray *dataSource2;
    
    NSArray *detailSource0;
    NSArray *detailSource1;
    
    NSArray *headerSource;
}

@end

@implementation PCSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.deckController.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PCSettingsHeaderCell *cell = (PCSettingsHeaderCell *)[tableView dequeueReusableCellWithIdentifier:@"PCSettingsHeaderCell"];
    cell.label.text = headerSource[section];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return SMALLCELLHEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return SMALLCELLHEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = 0;
    switch (section) {
        case 0:
            number = dataSource0.count;
            break;
        case 1:
            number = dataSource1.count;
            break;
        case 2:
            number = dataSource2.count;
            break;
        default:
            break;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if ((indexPath.section == 0) && (indexPath.row == 2 || indexPath.row == 4)) {
        cell = [self makeStandartCellAtIndexPath:indexPath];
    }
    else if (indexPath.section == 2) {
        cell = [self makeSwitchCellAtIndexPath:indexPath];
    }
    else {
        cell = [self makeDetailCellAtIndexPath:indexPath];
    }
    
//    UIView *view = [[UIView alloc] initWithFrame:cell.frame];
//    view.backgroundColor = kExtremelyDarkGreyColor;
//    cell.selectedBackgroundView = view;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (PCSettingsStandartCell*)makeStandartCellAtIndexPath:(NSIndexPath *)indexPath {
    PCSettingsStandartCell *cell = (PCSettingsStandartCell *)[self.tableView dequeueReusableCellWithIdentifier:@"PCSettingsStandartCell"];
    cell.label.text  = dataSource0[indexPath.row];
    cell.icon.image = [UIImage imageNamed:imageSource0[indexPath.row]];
    
    return cell;
}

- (PCSettingsDetailCell*)makeDetailCellAtIndexPath:(NSIndexPath *)indexPath {
    PCSettingsDetailCell *cell = (PCSettingsDetailCell *)[self.tableView dequeueReusableCellWithIdentifier:@"PCSettingsDetailCell"];
    if (indexPath.section == 0) {
        cell.label.text  = dataSource0[indexPath.row];
        cell.detailLabel.text = detailSource0[indexPath.row];
        cell.icon.image = [UIImage imageNamed:imageSource0[indexPath.row]];
    } else {
        cell.label.text  = dataSource1[indexPath.row];
        cell.detailLabel.text = detailSource1[indexPath.row];
        cell.icon.image = [UIImage imageNamed:imageSource1[indexPath.row]];
    }
    
    
    return cell;
}

- (PCSettingsSwitchCell*)makeSwitchCellAtIndexPath:(NSIndexPath *)indexPath {
    PCSettingsSwitchCell *cell = (PCSettingsSwitchCell *)[self.tableView dequeueReusableCellWithIdentifier:@"PCSettingsSwitchCell"];
    cell.label.text  = dataSource2[indexPath.row];
    if (indexPath.row == dataSource2.count - 1) {
        cell.cellSwitch.on = NO;
    }
    else {
        cell.cellSwitch.on = YES;
    }
    
    return cell;
}

#pragma mark - Helpers
- (void)setup {
    
    self.navigationItem.title = TITLEBARSETTINGS;
    [self makeBackButton];
    
    dataSource0 = @[@"Name, Surname", @"Email", @"Change photo", @"Change password", @"Log out"];
    dataSource1 = @[@"Unpair Pure.Charger"];
    dataSource2 = @[@"Incoming calls", @"Missed calls", @"Incoming messages", @"Email", @"Calendar events", @"Turn off all notifications"];
    
    detailSource0 = @[@"Grigoriy Samsonov",@"grigoriy.samsonov@gmail.com", @"", @"**********", @""];
    detailSource1 = @[@"Pure.Charger1"];
    
    imageSource0 = @[@"Icon_about", @"Icon_achievements", @"Icon_history", @"Icon_points", @"Icon_settings"];
    imageSource1 = @[@"Icon_store"];
    
    headerSource = @[@"PROFILE SETTINGS", @"DEVICE SETTINGS", @"NOTIFICATION SETTINGS"];
}

- (void)makeBackButton {
    UIBarButtonItem *item = [self makeMenuButton];
    [self.navigationItem setLeftBarButtonItem:item];
}


#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

@end
