//
//  PCAboutAppCell.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 11/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCAboutAppSection.h"

@implementation PCAboutAppSection

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
