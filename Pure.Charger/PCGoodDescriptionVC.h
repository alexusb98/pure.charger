//
//  PCGoodDescriptionVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"

@interface PCGoodDescriptionVC : PCBaseVC


@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sloganLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *workLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *workTimeLabel;

- (IBAction)buyButtonTap:(id)sender;


@property (strong, nonatomic) NSDictionary *dataSource;

@end
