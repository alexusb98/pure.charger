//
//  PCConnectView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCConnectView.h"

@interface PCConnectView ()

@end

@implementation PCConnectView

+ (PCConnectView *) loadConnectView {
    PCConnectView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCConnectView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];
    }
    
    return view;
}

@end
