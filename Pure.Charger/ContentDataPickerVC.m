//
//  ContentDataPickerVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "ContentDataPickerVC.h"


#define kPCVideosFolder @"it should be path here"

@interface ContentDataPickerVC ()

@end

@implementation ContentDataPickerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (id)initWithType:(ContentType)contentType nibNameOrNil:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentContentType = contentType ;
    }
    return self;
}

- (id)initWithType:(ContentType)contentType forObjectId:(NSString *)objectId nibNameOrNil:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentContentType = contentType ;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    currentContentType = kImageContentType;
    
    capturedImage = nil;
    capturedMediaURL = nil;
    
    [self addContentData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Picture adding
- (void)addContentData {
    NSString *title;
    switch (currentContentType) {
        case kVideoContentType:
            title = NSLocalizedString(@"Select Video", nil);
            break;
        case kImageContentType:
            title = NSLocalizedString(@"Select Image", nil);
            break;
        case kMixedContentType:
            title = NSLocalizedString(@"Select Media", nil);
            break;
        default:
            break;
    }
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:title
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Camera", @""), NSLocalizedString(@"Photo library", @""), NSLocalizedString(@"Photos album", @""),nil];
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    actionSheet.tag = kContentSource;
}


#pragma mark ActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:{
            if (currentContentType == kVideoContentType) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:NSLocalizedString(@"RightPosition", nil)
                                                                   delegate:nil
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                alertView.tag = kContentPhonePosition;
                alertView.delegate = self;
                [alertView show];
            }
            else {
                [self takePictureFromCamera];
            }
        }
            break;
        case 1:
            [self takePictureFromFile: UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        case 2:
            [self takePictureFromFile: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
            break;
        case 3:
            //            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }

    
    
//    switch (buttonIndex) {
//        case 0:
//            [self takePictureFromCamera];
//            break;
//        case 1:
//            [self takePictureFromFile: UIImagePickerControllerSourceTypePhotoLibrary];
//            break;
//        case 2:
//            [self takePictureFromFile: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
//            break;
//        case 3:
////            [self.navigationController popViewControllerAnimated:YES];
//            [self dismissViewControllerAnimated:YES completion:nil];
//            break;
//        default:
//            break;
//    }
}

#pragma mark AlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (alertView.tag) {
        case kContentPhonePosition:
            [self takePictureFromCamera];
            break;
            
        default:
            break;
    }
}

- (NSInteger)getVideoQuality{

    NSInteger currentSetQuality = [[[NSUserDefaults standardUserDefaults] objectForKey:@"VideoQuality"] integerValue];
    
    switch (currentSetQuality) {
        case 2:
            return UIImagePickerControllerQualityTypeHigh;
            break;
        case 0:
            return UIImagePickerControllerQualityTypeLow;
            break;
        default:
            return UIImagePickerControllerQualityTypeMedium;
            break;
    }

    
    return UIImagePickerControllerQualityTypeMedium;
}

- (void) takePictureFromCamera {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imgController = [[UIImagePickerController alloc] init] ;
        imgController.delegate = self;
        imgController.sourceType =  UIImagePickerControllerSourceTypeCamera;
        imgController.cameraDevice  =  UIImagePickerControllerCameraDeviceFront;
        imgController.videoQuality = [self getVideoQuality];

        switch (currentContentType) {
            case kVideoContentType:
                imgController.mediaTypes = [NSArray arrayWithObject:@"public.movie"];
                break;
            case kImageContentType:
                imgController.mediaTypes = [NSArray arrayWithObject:@"public.image"];
                break;
            case kMixedContentType:
                imgController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                break;
            default:
                break;
        }
        [self presentViewController:imgController animated:YES completion:nil];
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Can't use camera!\n Will use Photos album instead.", nil)
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self takePictureFromFile: UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    }
}


- (void) takePictureFromFile:(UIImagePickerControllerSourceType)dataSource {
    
    if ([UIImagePickerController isSourceTypeAvailable:dataSource]) {
        UIImagePickerController *imgController = [[UIImagePickerController alloc] init] ;
        imgController.delegate = self;
        imgController.sourceType =  dataSource;

        switch (currentContentType) {
            case kVideoContentType:
                imgController.mediaTypes = [NSArray arrayWithObject:@"public.movie"];
                break;
            case kImageContentType:
                imgController.mediaTypes = [NSArray arrayWithObject:@"public.image"];
                break;
            case kMixedContentType:
                imgController.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                break;
            default:
                break;
        }
        [self presentViewController:imgController animated:YES completion:nil];
    }
    else {
        NSString *message = nil;
        if (dataSource == UIImagePickerControllerSourceTypePhotoLibrary) {
            message = NSLocalizedString(@"Can't use photo library!", nil);
        } else if(dataSource == UIImagePickerControllerSourceTypeSavedPhotosAlbum){
            message = NSLocalizedString(@"Can't use Photos album!", nil);
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }
}


#pragma mark ImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    switch (picker.sourceType) {
        case UIImagePickerControllerSourceTypePhotoLibrary:
            
            if ([info objectForKey:UIImagePickerControllerMediaURL]) {//video from library
                capturedMediaURL = [info objectForKey:UIImagePickerControllerMediaURL] ;
                [self handleVideoWithURL:capturedMediaURL];
            } else if ([info objectForKey:UIImagePickerControllerReferenceURL]) {//image from library
                capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage] ;
                [self handleImage:capturedImage];
            }
            break;
        case UIImagePickerControllerSourceTypeSavedPhotosAlbum:
            
            if ([info objectForKey:UIImagePickerControllerMediaURL]) {//video from album
                capturedMediaURL = [info objectForKey:UIImagePickerControllerMediaURL] ;
                [self handleVideoWithURL:capturedMediaURL];
            } else if ([info objectForKey:UIImagePickerControllerReferenceURL]) {//image from album
                capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage] ;
                [self handleImage:capturedImage];
            }
            break;
        case UIImagePickerControllerSourceTypeCamera:
            if (picker.cameraCaptureMode == UIImagePickerControllerCameraCaptureModePhoto) {//image from camera
                capturedImage = [info objectForKey:UIImagePickerControllerOriginalImage] ;
                [self handleImage:capturedImage];
            }
            else if (picker.cameraCaptureMode == UIImagePickerControllerCameraCaptureModeVideo) {//video from camera
                capturedMediaURL = [info objectForKey:UIImagePickerControllerMediaURL] ;
                [self handleVideoWithURL:capturedMediaURL];
            }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:^(void){[self dismissViewControllerAnimated:YES completion:nil];}];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissViewControllerAnimated:YES completion:^(void){[self dismissViewControllerAnimated:YES completion:nil];}];
}
 
-(void) handleVideoWithURL:(NSURL*) videoURL {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH'-'mm'-'ss"];
        NSString* title = [NSString stringWithFormat:@"Skill_%@.mov",[formatter stringFromDate:[NSDate date]]];
        
        // Saving video
        NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [pathes objectAtIndex:0];
        
        NSString *videoDir = [[docsDir stringByAppendingPathComponent:kPCVideosFolder]stringByAppendingPathComponent:title];
        NSData *mediaData = [NSData dataWithContentsOfURL:videoURL];
        BOOL result = [mediaData writeToFile:videoDir atomically:YES];
        if (result != NO) {
            if ([_delegate respondsToSelector:@selector(contentDataPicker:didFinishSaveVideoWithURL:)]){
                [_delegate contentDataPicker:self didFinishSaveVideoWithURL:[NSURL fileURLWithPath:videoDir]];
            }
            
            if ([_delegate respondsToSelector:@selector(contentDataPicker:didFinishSaveVideoWithPath:)]){
                [_delegate contentDataPicker:self didFinishSaveVideoWithPath:videoDir];
            }
            
            NSLog(@"... and info is saved!");
        }
    });
}


-(void) handleImage:(UIImage*) image {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSString* title = nil;
//        if (currentObjectId) {
//            title = [NSString stringWithFormat:@"%@_bkg",currentObjectId];
//        } else {
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH'-'mm'-'ss"];
//            title = [NSString stringWithFormat:@"Image_%@",[formatter stringFromDate:[NSDate date]]];
//        }
        NSString *title = self.imageName;
        
        // Saving image
        NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [pathes objectAtIndex:0];
        NSString *imageDir = [NSString stringWithFormat:@"%@/%@", docsDir, title];
        NSData *mediaData = UIImageJPEGRepresentation(image, 1.0f);
        BOOL result = [mediaData writeToFile:imageDir atomically:YES];
        if (result != NO) {
            if ([_delegate respondsToSelector:@selector(contentDataPicker:didFinishSaveImageWithPath:)]){
                [_delegate contentDataPicker:self didFinishSaveImageWithPath:imageDir];
            }
            
            NSLog(@"... and info is saved!");
        }
    });
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
//        [self saveImageData:imageData forEventContent:content];
//        [content release];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            //            
//        });
//    });
    
}
/*
- (void) saveImageData:(NSData*)mediaData  {

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH'-'mm'-'ss"];
    NSString* title = [NSString stringWithFormat:@"Image_%@",[formatter stringFromDate:[NSDate date]]];
    [formatter release];

    // Saving image
    NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [pathes objectAtIndex:0];
    
    NSString *imagesDir = [[docsDir stringByAppendingPathComponent:kMSCImagesFolder]stringByAppendingPathComponent:title];
    
    BOOL result = [mediaData writeToFile:imagesDir atomically:YES];
    if (result != NO) {
        NSLog(@"... and info is saved!");
    }
    
}

- (void) saveVideoData:(NSData*)mediaData {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH'-'mm'-'ss"];
    NSString* title = [NSString stringWithFormat:@"Video_%@",[formatter stringFromDate:[NSDate date]]];
    [formatter release];

    // Saving image
    NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [pathes objectAtIndex:0];
    
    NSString *videoDir = [[docsDir stringByAppendingPathComponent:kMSCVideosFolder]stringByAppendingPathComponent:title];
    
    BOOL result = [mediaData writeToFile:videoDir atomically:YES];
    if (result != NO) {
        if (_delegate && [_delegate respondsToSelector:@selector(contentDataPicker:didFinishSaveVideoWithURL:)]){
            [_delegate contentDataPicker:self didFinishSaveVideoWithURL:[NSURL fileURLWithPath:videoDir]];
        }

        NSLog(@"... and info is saved!");
    }
 */
//////
    /*
    //Video crop
    NSURL *selectedVideoUrl = [NSURL URLWithString:imagesDir];
//    [info objectForKey:UIImagePickerControllerMediaURL];
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:selectedVideoUrl];
    CMTime videoDuration = playerItem.duration; //video duration
    float seconds = CMTimeGetSeconds(videoDuration);

    AVAsset *anAsset = [AVAsset assetWithURL:selectedVideoUrl];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetLowQuality];
        
        exportSession.outputURL = selectedVideoUrl; //can I use the same URL?
        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        float startTime = seconds < 20 ? 0 : seconds - 20;
        CMTime start = CMTimeMakeWithSeconds(startTime, 600);
        CMTime duration = CMTimeMakeWithSeconds(20, 600);
        
        CMTimeRange range = CMTimeRangeMake(start, duration);
        
        exportSession.timeRange = range;
        
    }
//////
    
}
*/

@end
