//
//  PCHistoryContainerView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 07/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCHistoryContainerView.h"

@interface PCHistoryContainerView ()

@end

@implementation PCHistoryContainerView

+ (PCHistoryContainerView *)loadHistoryContainerView {
    PCHistoryContainerView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCHistoryContainerView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];
        
    }
    
    return view;
}

//- (void)awakeFromNib {
//    NSString* nibName = @"PCHistoryContainerView";
//    if ([[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil]) {
////        [self.view setFrame:[self bounds]];
//        [self addSubview:self.view];
//    }
//}

//-(void)awakeFromNib {
//    [[NSBundle mainBundle] loadNibNamed:@"PCHistoryContainerView" owner:self options:nil];
//    [self addSubview: self.contentView];
//}

@end
