//
//  PCSource.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 13/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


#pragma mark - Titles
extern NSString* const TITLEBARCREATEACCOUNT;
extern NSString* const TITLEBARUSERPOINTS;
extern NSString* const TITLEBARUSERHISTORY;
extern NSString* const TITLEBARSTORE;
extern NSString* const TITLEBARSETTINGS;
extern NSString* const TITLEBARABOUTAPP;
extern NSString* const TITLEBARACHIEVEMENTS;

#pragma mark - Keys
extern NSString* const USERNAMEKEY;
extern NSString* const USERPASSWORDKEY;
extern NSString* const USEREMAILKEY;
extern NSString* const USERPICTUREPATHKEY;

#pragma mark - Alerts
extern NSString* const TITLEALERTERROR;
extern NSString* const TITLEALERTWRONGNAME;
extern NSString* const TITLEALERTWRONGEMAIL;
extern NSString* const TITLEALERTWRONGPASSWORD;

extern NSString* const TEXTALERTWRONGNAME;
extern NSString* const TEXTALERTWRONGPASSWORD;
extern NSString* const TEXTALERTWRONGDIGITS;
extern NSString* const TEXTALERTWRONGEMAIL;
extern NSString* const TEXTALERTWRONGPASSWORDLENGTH;
extern NSString* const TEXTALERTWRONGDATA;

extern NSString* const TEXTALERTDATASAVED;

extern NSString* const TEXTBONUSLABEL;

#pragma mark - Text
extern NSString* const TEXTCONNECTED;

#pragma mark - Sizes
extern const NSInteger SIDEBARWIDTH;
extern const NSInteger USERPASSWORDLENGTH;
extern const NSInteger HISTORYCELLHEIGHT;
extern const NSInteger SMALLCELLHEIGHT;
extern const NSInteger USERPOINTSCELLHEIGHT;
extern const NSInteger USERPOINTSBIGCELLHEIGHT;
extern const NSInteger STORECELLHEIGHT;
extern const NSInteger TABLEHEADERHEIGHT;
extern const NSInteger SECTIONHEADERHEIGHT;
extern const NSInteger TEXTFIELDBORDER;
extern const NSInteger CREATEACCOUNTBUTTONHEIGHT;
extern const NSInteger ADDPICTUREBUTTONWIDTH;
extern const NSInteger TRADEMARKLABELHEIGHT;
extern const NSInteger TITLELABELHEIGHT;

extern const NSInteger SMALLPOPUPWIDTH;
extern const NSInteger SMALLPOPUPHEIGHT;
extern const NSInteger MEDIUMPOPUPWIDTH;
extern const NSInteger MEDIUMPOPUPHEIGHT;
extern const NSInteger BIGPOPUPWIDTH;
extern const NSInteger BIGPOPUPHEIGHT;
extern const NSInteger SQUAREPOPUPWIDTH;
extern const NSInteger SQUAREPOPUPHEIGHT;

extern const NSInteger ITEMSOFFSET;
extern const NSInteger CIRCLERADIUS;
extern const NSInteger ARCWIDTH;
extern const NSInteger CIRCLEANIMATIONDURATION;
extern const NSInteger HISTORYBUTTONSOFFSET;
extern const NSInteger HISTORYMAPBUTTONOFFSET;

extern const NSInteger HISTORYCONTAINERHEIGHT;
extern const NSInteger VISIBLEHISTORYCONTAINERHEIGHT;
extern const NSInteger MINMAPHEIGHT;
