//
//  PCMenuVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCMenuVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCImageTextCell.h"
#import "PCHeaderCell.h"
#import "PCUserPointsVC.h"

@interface PCMenuVC () {
    NSArray *imageSource;
    NSArray *dataSource;
    NSArray *controllers;
}

@end

@implementation PCMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    PCHeaderCell *cell = (PCHeaderCell *)[tableView dequeueReusableCellWithIdentifier:@"PCHeaderCell"];
//
//    return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    CGFloat height = 0;
//    if (tableView == self.tableView) {
//        height = TABLEHEADERHEIGHT;
//    }
//    return height;
//}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = SMALLCELLHEIGHT;
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = dataSource.count;
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PCImageTextCell *cell = (PCImageTextCell *)[tableView dequeueReusableCellWithIdentifier:@"PCImageTextCell"];
    UIView *view = [[UIView alloc] initWithFrame:cell.frame];
    view.backgroundColor = kExtremelyDarkGreyColor;
    cell.selectedBackgroundView = view;
    
    cell.settingLabel.highlightedTextColor = [UIColor whiteColor];
    cell.settingLabel.text = dataSource[indexPath.row];
    cell.settingImageView.image = [UIImage imageNamed:imageSource[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:controllers[indexPath.row] ];
    [(UINavigationController*)self.deckController.centerController pushViewController:vc animated:NO];
    
    [self.deckController closeLeftViewAnimated:YES completion:^(IIViewDeckController *controller, BOOL success) {
        
    }];
    
    
}

#pragma mark - Helpers
- (void)setup {
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    
    dataSource = @[@"My points", @"Achievements", @"History", @"Store", @"Settings", @"About App"];
    controllers = @[@"PCMyPointsVC", @"PCAchievementsPC", @"PCUserHistoryVC", @"PCStoreVC", @"PCSettingsVC", @"PCAboutAppVC"];
    imageSource = @[@"Icon_points", @"Icon_achievements", @"Icon_history", @"Icon_store", @"Icon_settings", @"Icon_about"];
}

@end
