//
//  PCUserPointBigCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCUserPointBigCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image0;
@property (weak, nonatomic) IBOutlet UIImageView *image1;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UILabel *label0;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *point0;
@property (weak, nonatomic) IBOutlet UILabel *point1;
@property (weak, nonatomic) IBOutlet UILabel *point2;
@property (weak, nonatomic) IBOutlet UIButton *gotoButton;

@end
