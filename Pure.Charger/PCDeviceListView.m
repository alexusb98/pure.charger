//
//  PCDeviceListView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 10/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCDeviceListView.h"

@interface PCDeviceListView ()

@end

@implementation PCDeviceListView

+ (PCDeviceListView *) loadDeviceListView {
    PCDeviceListView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCDeviceListView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];
    }
    
    return view;
}


@end
