//
//  PCUserPointsView.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCUserPointsView : UIView

@property (weak, nonatomic) IBOutlet UIButton *okButton;

+ (PCUserPointsView *) loadUserPointsView;

@end
