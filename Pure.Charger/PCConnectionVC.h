//
//  PCConnectionVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"


@interface PCConnectionVC : PCBaseVC <IIViewDeckControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *label;

- (IBAction)sendEmail:(id)sender;

@end
