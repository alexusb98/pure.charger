//
//  PCAchievementRowView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 24/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCAchievementRowView.h"

@interface PCAchievementRowView ()

@end

@implementation PCAchievementRowView

+ (PCAchievementRowView *) loadAchievementRowView {
    PCAchievementRowView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCAchievementRowView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];

    }
    
    return view;
}

@end
