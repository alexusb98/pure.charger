//
//  PCUserPointCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 16/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCUserPointCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *resultImageView;

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
