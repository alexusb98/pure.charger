//
//  PCHistoryCell.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 07/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCHistoryCell.h"

@implementation PCHistoryCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
