//
//  PCUserHistoryVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import <MapKit/MapKit.h>


@interface PCUserHistoryVC : PCBaseVC <IIViewDeckControllerDelegate, MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@property (weak, nonatomic) IBOutlet UIButton *containerButton;

- (IBAction)mapButtonTap:(id)sender;
- (IBAction)containerButtonTap:(id)sender;

@end
