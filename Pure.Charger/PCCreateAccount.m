//
//  PCCreateAccount.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCCreateAccount.h"
#import "PCSource.h"
#import "PCConstants.h"

#define kBarHeight 64

@interface PCCreateAccount (){
    BOOL keyboardVisible;
    CGFloat keyboardHeight;
    CALayer *bottomBorder1;
    CALayer *bottomBorder2;
    CALayer *bottomBorder3;
}


@end

@implementation PCCreateAccount

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Keyboard show/hide notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidHide:)
                                                 name: UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard Notifications
- (void)keyboardDidShow:(NSNotification *)notification {
    // If keyboard is visible, return
    if (keyboardVisible) {
        return;
    }
    
    //Keyboard height
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrame = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrame CGRectValue];
    keyboardHeight = CGRectGetHeight(keyboardFrameRect);
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame) + keyboardHeight);
    
    // Keyboard is now visible
    keyboardVisible = YES;
    
}

- (void)keyboardDidHide:(NSNotification *)notification {
    // Is the keyboard already shown
    if (!keyboardVisible) {
        return;
    }
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame));
                     }
                     completion:^(BOOL result){
        
                     }];
//    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame));
    
    // Keyboard is no longer visible
    keyboardVisible = NO;
}

#pragma mark - textField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField) {
        if (![self checkUserName]) {
            [self showAlertInViewController:self withTitle:TITLEALERTWRONGNAME message:TEXTALERTWRONGDIGITS];
            self.nameTextField.text = @"";
        } else {
            [self.emailTextField becomeFirstResponder];
        }
    }
    else if (textField == self.emailTextField) {
        if (![self checkUserEmail]) {
            [self showAlertInViewController:self withTitle:TITLEALERTWRONGEMAIL message:TEXTALERTWRONGEMAIL];
            self.emailTextField.text = @"";
        } else {
            [self.passwordTextField becomeFirstResponder];
        }
    }
    else if (textField == self.passwordTextField){
        if (![self checkUserPassword]) {
            [self showAlertInViewController:self withTitle:TITLEALERTWRONGPASSWORD message:TEXTALERTWRONGPASSWORD];
            self.passwordTextField.text = @"";
        } else {
            [self hideKeyboard];
        }
        
    }
    
    return YES;
}

#pragma mark - ContentDataPickerVC Delegate
- (void)contentDataPicker:(ContentDataPickerVC *)contentDataPickerInstance didFinishSaveImageWithPath:(NSString *)mediaPath {
    
    self.userImageView.image = [UIImage imageNamed:mediaPath];
    [self saveProfileImagePath:mediaPath];
}

#pragma mark - Helpers
- (BOOL)checkUserName {
    
    BOOL result = YES;
    
    NSString *regex = @"^[a-zA-Z]+$";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    result = [test evaluateWithObject:self.nameTextField.text];
    
    return result;
}

- (BOOL)checkUserEmail{
    
    BOOL result = YES;

    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    result = [emailTest evaluateWithObject:self.emailTextField.text];
    
    return result;
}

- (BOOL)checkUserPassword {
    
    BOOL result = YES;
    if ([self.passwordTextField.text length] < USERPASSWORDLENGTH) {
        result = NO;
    }
    return result;
}

- (void)setup {
    
    //navigationBar
//    [self.navigationController.navigationBar setBarTintColor:kExtremelyDarkGreyColor];
    [self customizeNavigationBar];
    self.navigationItem.title = TITLEBARCREATEACCOUNT;
    [self makeRightButton];
    
    //textFields
    [self makeBottomBorders];
    [self.emailTextField.layer addSublayer:bottomBorder1];
    [self.passwordTextField.layer addSublayer:bottomBorder2];
    [self.nameTextField.layer addSublayer:bottomBorder3];
    
    NSAttributedString *emailHolder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{ NSForegroundColorAttributeName : kLightGreenColor }];
    self.emailTextField.attributedPlaceholder = emailHolder;
    
    NSAttributedString *passwordHolder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{ NSForegroundColorAttributeName : kLightGreenColor }];
    self.passwordTextField.attributedPlaceholder = passwordHolder;
    
    NSAttributedString *nameHolder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{ NSForegroundColorAttributeName : kLightGreenColor }];
    self.nameTextField.attributedPlaceholder = nameHolder;
    
    [self addConstraints];
    
    //title
    NSString *string = self.titleLabel.text;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttribute:NSFontAttributeName
                      value:k29BoldFont
                      range:NSMakeRange(5, 7)];
    self.titleLabel.text = @"";
    self.titleLabel.attributedText = attString;
}

- (void)makeRightButton {
    UIImage *buttonImage = [[UIImage imageNamed:@"doneButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:buttonImage style:UIBarButtonItemStylePlain target:self action:@selector(tapDone:)];
//    [item setTitle:@"Done"];
//    [item setTintColor:kNavigationBarTintColor];
    
    [self.navigationItem setRightBarButtonItem:item];
}


- (UIView *)findFirstResonder:(UIView*)root{
    if ([root isFirstResponder]) {
        return root;
    }
    
    for (UIView *subView in root.subviews) {
        UIView *firstResponder = [self findFirstResonder:subView];
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    return nil;
}

- (NSString*)userName {
    NSString *name;
    if ([self.nameTextField.text length] > 0) {
        name = self.nameTextField.text;
    } else {
        name = [self loadProfileName];
    }
    
    if (!name) {
        name = @"user";
    }
    
    return name;
}

- (void)hideKeyboard {
    [[self findFirstResonder:self.scrollView] endEditing:YES];
}

- (void)makeBottomBorders {
    bottomBorder1 = [CALayer layer];
    bottomBorder2 = [CALayer layer];
    bottomBorder3 = [CALayer layer];
    CGRect frame = CGRectMake(CGRectGetMinX(self.emailTextField.frame),
                              CGRectGetHeight(self.emailTextField.frame) - TEXTFIELDBORDER,
                              [self screenWidth], TEXTFIELDBORDER);
    bottomBorder1.frame = frame;
    bottomBorder1.backgroundColor = kLightGreyColor.CGColor;
    
    bottomBorder2.frame = frame;
    bottomBorder2.backgroundColor = kLightGreyColor.CGColor;
    
    bottomBorder3.frame = frame;
    bottomBorder3.backgroundColor = kLightGreyColor.CGColor;
    
}

- (void)addConstraints {
    
    self.addPictureButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.trademarkLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.userImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.nameTextField.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.scrollView addSubview:self.addPictureButton];
    [self.scrollView addSubview:self.trademarkLabel];
    [self.scrollView addSubview:self.titleLabel];
    [self.scrollView addSubview:self.userImageView];
    [self.scrollView addSubview:self.nameTextField];
    
    NSArray *verticalConstraintsUserImageView =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-distance1-[view1(height1)]-distance2-[view2]"
                                            options:0
                                            metrics:@{
                                                      @"distance1" : @(-20),
                                                      @"height1" : @(200),
                                                      @"distance2" : @(30)
                                                      }
                                              views:@{
                                                      @"view1" : self.userImageView,
                                                      @"view2" : self.nameTextField
                                                      }];
    
    [self.scrollView addConstraints:verticalConstraintsUserImageView];
    
    NSArray *horizontalConstraintsUserImageView =
    [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(width)]-0-|"
                                            options:0
                                            metrics:@{
                                                      @"width" : @(self.screenWidth + 10)
                                                      }                                              views:@{
                                                                                                             @"view" : self.userImageView
                                                                                                             }];
    [self.scrollView addConstraints:horizontalConstraintsUserImageView];
    
//    NSArray *verticalConstraintsAddPictureButton =
//    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view(height)]"
//                                            options:0
//                                            metrics:@{
//                                                      @"height" : @(CGRectGetHeight(self.userImageView.frame) + kBarHeight)
//                                                      }
//                                              views:@{
//                                                      @"view" : self.userImageView
//                                                      }];
//    
//    [self.scrollView addConstraints:verticalConstraintsAddPictureButton];
    
    
//    self.userImageView.frame = CGRectMake(CGRectGetMinX(self.userImageView.frame), CGRectGetMinY(self.userImageView.frame), [self screenWidth], CGRectGetHeight(self.userImageView.frame));
/*    NSArray *horizontalConstraints0 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view(width)]-0-|"
                                            options:0
                                            metrics:@{
                                                      @"width" : @(self.screenWidth)
                                                      }                                              views:@{
                                                      @"view" : self.userImageView
                                                      }];
    [self.scrollView addConstraints:horizontalConstraints0];
    
    NSArray *verticalConstraints0 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view(height)]"
                                            options:0
                                            metrics:@{
                                                      @"height" : @(CGRectGetHeight(self.userImageView.frame) + kBarHeight)
                                                      }
                                              views:@{
                                                      @"view" : self.userImageView
                                                      }];
    
    [self.scrollView addConstraints:verticalConstraints0];
*/
 
//    distance = [self screenHeight] - TITLELABELHEIGHT - TRADEMARKLABELHEIGHT - kBarHeight - 30;
    NSArray *verticalConstraintsAddPictureButton =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-distance-[view]"
                                            options:0
                                            metrics:@{
                                                      @"distance" : @(170 - 20)
                                                      }
                                              views:@{
                                                      @"view" : self.addPictureButton
                                                      }];
    
    [self.scrollView addConstraints:verticalConstraintsAddPictureButton];
    
    CGFloat distance = [self screenWidth] - ADDPICTUREBUTTONWIDTH - 25;
    
    NSArray *horizontalConstraintsAddPictureButton =
    [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-distance-[view]"
                                            options:0
                                            metrics:@{
                                                      @"distance" : @(distance)
                                                      }
                                              views:@{
                                                      @"view" : self.addPictureButton
                                                      }];
    [self.scrollView addConstraints:horizontalConstraintsAddPictureButton];
    
    distance = [self screenHeight] - TITLELABELHEIGHT - TRADEMARKLABELHEIGHT - kBarHeight - 30;
    NSArray *verticalConstraints =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-distance-[view1]-0-[view2]"
                                            options:0
                                            metrics:@{
                                                      @"distance" : @(distance)
                                                      }
                                              views:@{
                                                      @"view1" : self.titleLabel,
                                                      @"view2" : self.trademarkLabel
                                                      }];
    
    [self.scrollView addConstraints:verticalConstraints];
    

    
}

#pragma mark - Actions

- (IBAction)addPicture:(id)sender {
    
//    ContentDataPickerVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ContentDataPickerVC" ];
//    vc.delegate = self;
//    vc.imageName = [self userName];
//    
//    [self presentViewController:vc animated:YES completion:^{}];

}

- (void)tapDone:(id)sender {
//    if (self.nameTextField.text.length == 0 ||
//        self.passwordTextField.text.length == 0 ||
//        self.emailTextField.text.length == 0) {
//        [self showAlertInViewController:self withTitle:TITLEALERTERROR message:TEXTALERTWRONGDATA];
//    } else {
//        [self saveProfileName:self.nameTextField.text];
//        [self saveProfileEmail:self.emailTextField.text];
//        [self saveProfilePassword:self.passwordTextField.text];
//        [self showAlertInViewController:self withTitle:nil message:TEXTALERTDATASAVED];
//    }
    [self showAlertInViewController:self withTitle:nil message:TEXTALERTDATASAVED];
}

@end
