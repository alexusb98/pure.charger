//
//  main.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
