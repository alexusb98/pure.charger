//
//  PCConstants.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#define k10Font [UIFont fontWithName:@"HelveticaNeue" size:10]
#define k14Font [UIFont fontWithName:@"HelveticaNeue" size:14]
#define k20Font [UIFont fontWithName:@"HelveticaNeue" size:20]

#define k29BoldFont [UIFont fontWithName:@"Helvetica-Bold" size:29]


#define kLightGreyColor [UIColor colorWithRed:188./255. green:190./255. blue:192./255. alpha:1.]
#define kDarkGreyColor [UIColor colorWithRed:48./255. green:48./255. blue:48./255. alpha:1.]
#define kVeryDarkGreyColor [UIColor colorWithRed:39./255. green:39./255. blue:39./255. alpha:1.]
#define kExtremelyDarkGreyColor [UIColor colorWithRed:25./255. green:25./255. blue:25./255. alpha:1.]
//#define kAlmostBlackColor [UIColor colorWithRed:25./255. green:25./255. blue:25./255. alpha:1.]
#define kAlmostBlackColor [UIColor colorWithRed:19./255. green:19./255. blue:19./255. alpha:1.]
#define kLightGreenColor [UIColor colorWithRed:17./255. green:205./255. blue:132./255. alpha:1.]