//
//  PCStoreVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCStoreVC.h"
#import "PCGoodDescriptionVC.h"
#import "PCSource.h"
#import "PCStoreCell.h"

@interface PCStoreVC () {
    NSArray *dataPointsSource;
    NSArray *dataTitleSource;
    NSArray *dataImageSource;
    NSArray *dataDescriptionSource;
}

@end

@implementation PCStoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return STORECELLHEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataTitleSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PCStoreCell *cell = (PCStoreCell *)[tableView dequeueReusableCellWithIdentifier:@"PCStoreCell"];
    cell.titleLabel.text = dataTitleSource[indexPath.row];
    cell.pointsLabel.text = dataPointsSource[indexPath.row];
    cell.descriptionLabel.text = dataDescriptionSource[indexPath.row];
    
    cell.getItButton.tag = indexPath.row;
    cell.moreInfoButton.tag = indexPath.row;
    
    [cell.getItButton addTarget:self action:@selector(tapGetItButtonAtIndexPath:) forControlEvents:UIControlEventTouchUpInside];
    [cell.moreInfoButton addTarget:self action:@selector(tapMoreInfoButtonAtIndexPath:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
}

#pragma mark - Helpers
- (void)setup {
    [self makeBackButton];
    self.navigationItem.title = TITLEBARSTORE;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

//    dataImageSource = ;
    dataPointsSource = @[@"1500 PTS", @"1000 PTS", @"2000 PTS"];
    dataTitleSource = @[@"KASPERSKY INTERNET SECURITY MULTI-DEVICE", @"KASPERSKY SAFE KIDS", @"KASPERSKY TOTAL SECURITY MULTI-DEVICE"];
    dataDescriptionSource = @[@"Relible and convenient solution to protect your personal data on the Inernet.", @"Assistance in education contenporary children. Healthy communication, proper development, mutual understanding.", @"Maximum protection of your finances, privacy, family and personal data on the Internet."];
    
}

- (void)makeBackButton {
    
    UIImage *backButtonImage = [[UIImage imageNamed:@"NavigationBarButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(tapBack:)];
    [item setTitle:@"Menu"];
    //    [item setTintColor:kNavigationBarTintColor];
    
    [self.navigationItem setLeftBarButtonItem:item];
}


//- (PCUserPointOrdinaryCell*)makeUserPointOrdinaryCellInTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
//    PCUserPointOrdinaryCell *cell = (PCUserPointOrdinaryCell *)[tableView dequeueReusableCellWithIdentifier:@"PCUserPointOrdinaryCell"];
//    
//    NSArray *data = dataLabelSource [[NSString stringWithFormat: @"%ld", indexPath.section] ];
//    cell.label0.text = data[0];
//    cell.label1.text = data[1];
//    cell.label2.text = data[2];
//    
//    return cell;
//}
//
//- (PCUserPointBigCell*)makeUserPointBigCellnTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
//    PCUserPointBigCell*cell = (PCUserPointBigCell *)[tableView dequeueReusableCellWithIdentifier:@"PCUserPointBigCell"];
//    
//    NSArray *data = dataLabelSource [[NSString stringWithFormat: @"%ld", indexPath.section] ];
//    cell.label0.text = data[0];
//    cell.label1.text = data[1];
//    cell.label2.text = data[2];
//    
//    cell.point0.text = dataPointsSource[0];
//    cell.point1.text = dataPointsSource[1];
//    cell.point2.text = dataPointsSource[2];
//    
//    [cell.gotoButton addTarget:self action:@selector(tapGotoButton) forControlEvents:UIControlEventTouchUpInside];
//    return cell;
//}

#pragma mark - Actions

- (void)tapBack:(id)sender {
    //    self.deckController.leftSize = self.screenWidth - SIDEBARWIDTH;
    self.deckController.enabled = YES;
    [self.deckController toggleLeftViewAnimated:YES];
}

- (void)tapGetItButtonAtIndexPath:(UIButton *)sender {
    
}

- (void)tapMoreInfoButtonAtIndexPath:(UIButton *)sender {
    
    PCGoodDescriptionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCGoodDescriptionVC"];
    vc.dataSource = @{
    @"image": @"",
    @"title": @"Kaspersky Safe Kids",
    @"slogan": @"This is the best sowtware you ever dreamed about.",
    @"points": @"400 PTS",
    @"description": @"Very goooooooood software. Make friends and use it now. Press the button.",
    @"city": @"Saint-Petersburg",
    @"work": @"Working time",
    @"address": @"Nevsky prospect, 100",
    @"workTime": @"Mo-Fr; 10 am - 7pm"};
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}


@end
