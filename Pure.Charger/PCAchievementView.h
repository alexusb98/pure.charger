//
//  PCAchievementView.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 21/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCAchievementView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

+ (PCAchievementView *) loadAchievementView;

+ (void)showCircleAnimationInView:(UIView*)view;

+ (void)showSVGAnimationFromFile:(NSString*)fileName inView:(UIView*)view;

@end
