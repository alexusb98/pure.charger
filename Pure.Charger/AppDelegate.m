//
//  AppDelegate.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "AppDelegate.h"
#import "PCLogonVC.h"
#import "PCMenuVC.h"

@interface AppDelegate (){
    UIViewController *firstVC;
}

@property (strong, nonatomic) UIViewController *centerController;
@property (strong, nonatomic) UIViewController *leftController;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UINavigationController *initVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PCNavigationController"];
    
    firstVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"PCLogonVC" ];
    NSArray *controllers = @[firstVC];
    [initVC setViewControllers: controllers];
    // deck
    PCMenuVC* leftController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PCMenuVC"];
    
    self.deckController =  [[IIViewDeckController alloc] initWithCenterViewController:initVC
                                                                   leftViewController:leftController
                                                                  rightViewController:nil];
    
    self.deckController.enabled = NO;
    self.leftController = self.deckController.leftController;
    self.centerController = self.deckController.centerController;
    self.window.rootViewController = self.deckController;
    // deck

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
