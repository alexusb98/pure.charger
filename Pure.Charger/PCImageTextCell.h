//
//  PCImageTextCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCImageTextCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *settingImageView;
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;

@end
