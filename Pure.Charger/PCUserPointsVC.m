//
//  PCUserPointsVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCUserPointsVC.h"
#import "PCStoreVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCUserPointsView.h"
#import "PCUserPointOrdinaryCell.h"
#import "PCUserPointBigCell.h"

@interface PCUserPointsVC () {
    PCUserPointsView *deviceView;
    NSDictionary *dataImageSource;
    NSDictionary *dataLabelSource;
    NSArray *dataPointsSource;
    NSArray *dataHeaderSource;
}

@end

@implementation PCUserPointsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self makeDeviceView];
    [self.view addSubview:deviceView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = CGRectMake(0, 0, [self screenWidth], 10);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = k10Font;
    
    switch (section) {
        case 0:
            label.backgroundColor = [UIColor redColor];
            break;
        case 1:
            label.backgroundColor = [UIColor greenColor];
            break;
        case 2:
            label.backgroundColor = [UIColor grayColor];
            break;
        default:
            break;
    }
    label.text = dataHeaderSource[section];
    
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return SECTIONHEADERHEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    if (indexPath.section == 0) {
        height = USERPOINTSBIGCELLHEIGHT ;
    } else {
        height = USERPOINTSCELLHEIGHT;
    }
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [self makeUserPointBigCellnTableView:tableView atIndexPath:indexPath];
    } else {
        cell = [self makeUserPointOrdinaryCellInTableView:tableView atIndexPath:indexPath];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
  
    
    
}

#pragma mark - Helpers
- (void)setup {
    [self makeBackButton];
    self.navigationItem.title = TITLEBARUSERPOINTS;
//    [self makeDeviceView];
//    [self.view addSubview:deviceView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    NSArray *label0 = @[@"KASPERSKY INTERNET SECURITY MULTI-DEVICE", @"KASPERSKY SAFE KIDS", @"KASPERSKY TOTAL SECURITY MULTI-DEVICE"];
    NSArray *label1 = @[@"2X SUPER RECHARGE", @"LIFEGUARD CHARGER", @"SECURE CHARGE"];
    NSArray *label2 = @[@"CHARGER NINJA", @"INTER- CONTINENTAL", @"SAFETY SCOUT"];
    dataLabelSource = @{@"0":label0, @"1":label1, @"2":label2};
    
    dataPointsSource = @[@"1500 PTS", @"1000 PTS", @"2000 PTS"];
    dataHeaderSource = @[@"SPECIAL OFFERS", @"MY ACHIEVEMENTS", @"MORE ACHIEVEMENTS"];
    
}

- (void)makeBackButton {
    
    UIImage *backButtonImage = [[UIImage imageNamed:@"NavigationBarButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(tapBack:)];
    [item setTitle:@"Menu"];
    //    [item setTintColor:kNavigationBarTintColor];
    
        [self.navigationItem setLeftBarButtonItem:item];
}

- (void)makeDeviceView {
    if (deviceView) {
        return;
    }
    
    deviceView = [PCUserPointsView loadUserPointsView];
    CGRect frame = CGRectMake([self screenWidth]/2 - BIGPOPUPWIDTH/2, BIGPOPUPHEIGHT/4, BIGPOPUPWIDTH, BIGPOPUPHEIGHT);
    deviceView.frame = frame;
    deviceView.backgroundColor = [UIColor greenColor];
    
    [deviceView.okButton addTarget:self action:@selector(tapOKButton) forControlEvents:UIControlEventTouchUpInside];
}

- (PCUserPointOrdinaryCell*)makeUserPointOrdinaryCellInTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    PCUserPointOrdinaryCell *cell = (PCUserPointOrdinaryCell *)[tableView dequeueReusableCellWithIdentifier:@"PCUserPointOrdinaryCell"];
    
    NSArray *data = dataLabelSource [[NSString stringWithFormat: @"%ld", indexPath.section] ];
    cell.label0.text = data[0];
    cell.label1.text = data[1];
    cell.label2.text = data[2];

    return cell;
}

- (PCUserPointBigCell*)makeUserPointBigCellnTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    PCUserPointBigCell*cell = (PCUserPointBigCell *)[tableView dequeueReusableCellWithIdentifier:@"PCUserPointBigCell"];
    
    NSArray *data = dataLabelSource [[NSString stringWithFormat: @"%ld", indexPath.section] ];
    cell.label0.text = data[0];
    cell.label1.text = data[1];
    cell.label2.text = data[2];
    
    cell.point0.text = dataPointsSource[0];
    cell.point1.text = dataPointsSource[1];
    cell.point2.text = dataPointsSource[2];
    
    [cell.gotoButton addTarget:self action:@selector(tapGotoButton) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - Actions

- (void)tapBack:(id)sender {
    //    self.deckController.leftSize = self.screenWidth - SIDEBARWIDTH;
    self.deckController.enabled = YES;
    [self.deckController toggleLeftViewAnimated:YES];
}

- (void)tapOKButton {
    [deviceView removeFromSuperview];
}

- (void)tapGotoButton {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCStoreVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}



@end
