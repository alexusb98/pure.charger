//
//  PCHistoryCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 07/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCHistoryCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *historyImage;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
