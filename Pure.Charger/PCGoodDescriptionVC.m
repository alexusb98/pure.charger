//
//  PCGoodDescriptionVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCGoodDescriptionVC.h"

@interface PCGoodDescriptionVC ()

@end

@implementation PCGoodDescriptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    
    self.titleLabel.text = self.dataSource[@"title"];
    self.sloganLabel.text = self.dataSource[@"slogan"];
    self.pointsLabel.text = self.dataSource[@"points"];
    self.descriptionLabel.text = self.dataSource[@"description"];
    self.cityLabel.text = self.dataSource[@"city"];
    self.workLabel.text = self.dataSource[@"work"];
    self.addressLabel.text = self.dataSource[@"address"];
    self.workTimeLabel.text = self.dataSource[@"workTime"];

}


- (IBAction)buyButtonTap:(id)sender {
}
@end
