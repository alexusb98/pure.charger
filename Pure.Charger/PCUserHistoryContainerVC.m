//
//  PCUserHistoryContainerVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 06/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCUserHistoryContainerVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCHistoryCell.h"

@interface PCUserHistoryContainerVC () {
    NSArray *imageSource;
    NSArray *addressSource;
    NSArray *placeSource;
    NSArray *dateSource;
    NSArray *timeSource;
}

@end

@implementation PCUserHistoryContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    self.tableView.contentSize = CGSizeMake(CGRectGetWidth(self.tableView.frame), 500);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    imageSource = @[@"ic_map_pin_red", @"ic_map_pin", @"ic_map_pin", @"ic_map_pin"];
    addressSource = @[@"Ostrovskogo, sq., 12", @"Nevsky, pr., 1", @"Kupchinskaya, str., 29", @"Kupchinskaya, str., 21"];
    placeSource = @[@"Super recharge point", @"Pyaterochka shop", @"Laryek babi Zini", @"Laryek babi Zini"];
    dateSource = @[@"12/02/2016", @"12/02/2016", @"12/02/2016", @"12/02/2016"];
    timeSource = @[@"2:09 PM", @"2:09 PM", @"5:00 AM", @"4:45 AM"];
    
    [self placeContainers];
//    self.topBorderView.alpha = 0;
}

- (void)placeContainers {
    
    PCHistoryContainerView *view = [PCHistoryContainerView loadHistoryContainerView];
    view.containerImage.image = [UIImage imageNamed:@"history-charge_time"];
    view.containerText.attributedText = [self makeAttributedStringWithText:@"15 min"];
    [self.topView addSubview:view];
    
    PCHistoryContainerView *view2 = [PCHistoryContainerView loadHistoryContainerView];
    view2.containerImage.image = [UIImage imageNamed:@"history-ohm"];
    view2.containerText.attributedText = [self makeAttributedStringWithText:@"0,7 A"];
    [self.topView addSubview:view2];
    
    PCHistoryContainerView *view3 = [PCHistoryContainerView loadHistoryContainerView];
    view3.containerImage.image = [UIImage imageNamed:@"history-volt"];
    view3.containerText.attributedText = [self makeAttributedStringWithText:@"12 volt"];
    [self.topView addSubview:view3];
    
    PCHistoryContainerView *view4 = [PCHistoryContainerView loadHistoryContainerView];
    view4.containerImage.image = [UIImage imageNamed:@"history-pts"];
    view4.containerText.attributedText = [self makeAttributedStringWithText:@"400 pts"];
    view4.borderView.alpha = 0;
    [self.topView addSubview:view4];
    
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view2.translatesAutoresizingMaskIntoConstraints = NO;
    view3.translatesAutoresizingMaskIntoConstraints = NO;
    view4.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray *horizontalConstraints0 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[view1]-0-[view2(view1)]-0-[view3(view1)]-0-[view4(view1)]-20-|"
                                            options:0
                                            metrics:0
                                              views:@{
                                                      @"view1" : view,
                                                      @"view2" : view2,
                                                      @"view3" : view3,
                                                      @"view4" : view4
                                                      }];
    [self.topView addConstraints:horizontalConstraints0];
    
    
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[view(height)]" options:0 metrics:@{@"height" : @(HISTORYCONTAINERHEIGHT)} views:@{@"view" : view}]];
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[view(height)]" options:0 metrics:@{@"height" : @(HISTORYCONTAINERHEIGHT)} views:@{@"view" : view2}]];
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[view(height)]" options:0 metrics:@{@"height" : @(HISTORYCONTAINERHEIGHT)} views:@{@"view" : view3}]];
    [self.topView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-75-[view(height)]" options:0 metrics:@{@"height" : @(HISTORYCONTAINERHEIGHT)} views:@{@"view" : view4}]];
}

- (NSMutableAttributedString*)makeAttributedStringWithText:(NSString*)text{
//    NSString *string = [NSString stringWithFormat:@"%@%@", digits, text];
    NSRange position = [text rangeOfString:@" "];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:kLightGreenColor
                      range:NSMakeRange(0, position.location)];
    
    return attString;
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = imageSource.count;
    
    return number;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return HISTORYCELLHEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PCHistoryCell *cell = (PCHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"PCHistoryCell"];
    UIView *view = [[UIView alloc] initWithFrame:cell.frame];
    view.backgroundColor = kExtremelyDarkGreyColor;
    cell.selectedBackgroundView = view;
    
    
    cell.historyImage.image = [UIImage imageNamed:imageSource[indexPath.row]];
    cell.addressLabel.text = addressSource[indexPath.row];
    cell.placeLabel.text = placeSource[indexPath.row];
    cell.dateLabel.text = dateSource[indexPath.row];
    cell.timeLabel.text = timeSource[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
