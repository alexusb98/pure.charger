//
//  PCMyPointsVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 20/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import "UICountingLabel.h"

@interface PCMyPointsVC : PCBaseVC <IIViewDeckControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UICountingLabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *bonusLabel;
@property (weak, nonatomic) IBOutlet UIView *bonusView;


@end
