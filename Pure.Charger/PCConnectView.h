//
//  PCConnectView.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCConnectView : UIView

@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;


+ (PCConnectView *) loadConnectView;

@end
