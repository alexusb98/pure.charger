//
//  PCBaseVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "IIViewDeckController.h"

@class PCAchievementView;

@interface PCBaseVC : UIViewController
<MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) IIViewDeckController *deckController;

@property (nonatomic, assign) CGRect achievementViewHiddenFrame;
@property (nonatomic, assign) CGRect achievementViewShownFrame;
@property (nonatomic, strong) PCAchievementView *achievementView;
@property (nonatomic, strong)UIView *blackView;

- (CGFloat)screenWidth;
- (CGFloat)screenHeight;


- (void)setup;

- (void)customizeNavigationBar;

#pragma mark Profile
- (void)saveProfileName:(NSString*)name;
- (NSString*)loadProfileName;

- (void)saveProfilePassword:(NSString*)password;
- (NSString*)loadProfilePassword;

- (void)saveProfileEmail:(NSString*)email;
- (NSString*)loadProfileEmail;

- (void)saveProfileImagePath:(NSString*)path;
- (NSString*)loadProfileImagePath;

#pragma mark Alert
- (void)showAlertInViewController:(UIViewController*)viewController withTitle:(NSString*) title message:(NSString*) message;

#pragma mark Email
- (void)showEmailPageInViewController:(UIViewController*)viewController;

#pragma mark Button
- (UIBarButtonItem*)makeMenuButton;

#pragma mark AchievementView
- (void)initBlackView;
- (void)initAchievementViewFrames;
- (PCAchievementView*)makeAchievementViewWithSVGFile:(NSString*)fileName andTitle:(NSString*)title;
- (void)showAchievementViewWithSVGFile:(NSString*)fileName andTitle:(NSString*)title delay:(NSInteger)delay animatedLabel:(BOOL)animatedLabel;

@end
