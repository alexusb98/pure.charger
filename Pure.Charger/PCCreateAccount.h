//
//  PCCreateAccount.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import "ContentDataPickerVC.h"

@interface PCCreateAccount : PCBaseVC <UITextFieldDelegate, ContentDataPickerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *addPictureButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *trademarkLabel;


- (IBAction)addPicture:(id)sender;


@end
