//
//  ContentDataPickerVC.h
//


#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>

typedef enum{
    kContentPhonePosition = 211,
    kContentSource = 212
}ContentAlertType;

typedef enum{
    kVideoContentType,
    kImageContentType,
    kMixedContentType
}ContentType;

@class Event, Content;

@protocol ContentDataPickerDelegate;

@interface ContentDataPickerVC : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    UIImage *capturedImage;
    NSURL *capturedMediaURL;
    ContentType currentContentType;
    NSString *currentObjectId;
}

@property (strong, nonatomic)  NSString *imageName;

- (id)initWithType:(ContentType)contentType  nibNameOrNil:(NSString*)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
- (id)initWithType:(ContentType)contentType forObjectId:(NSString*)objectId nibNameOrNil:(NSString*)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@property (nonatomic, retain) id<ContentDataPickerDelegate> delegate;

@end


@protocol ContentDataPickerDelegate <NSObject>

@optional
- (void) contentDataPicker:(ContentDataPickerVC *)contentDataPickerInstance didFinishSaveVideoWithURL:(NSURL*)mediaURL;
- (void) contentDataPicker:(ContentDataPickerVC *)contentDataPickerInstance didFinishSaveVideoWithPath:(NSString*)mediaPath;
- (void) contentDataPicker:(ContentDataPickerVC *)contentDataPickerInstance didFinishSaveImageWithPath:(NSString*)mediaPath;

@end