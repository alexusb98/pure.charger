//
//  PCUserHistoryContainerVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 06/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import "PCHistoryContainerView.h"

@interface PCUserHistoryContainerVC : PCBaseVC <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *topBorderView;



@end
