//
//  PCSettingsVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import "ContentDataPickerVC.h"

@interface PCSettingsVC : PCBaseVC <UITableViewDataSource, UITableViewDelegate, IIViewDeckControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
