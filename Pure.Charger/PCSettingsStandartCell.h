//
//  PCSettingsStandartCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 28/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCSettingsStandartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
