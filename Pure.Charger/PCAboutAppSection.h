//
//  PCAboutAppCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 11/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCAboutAppSection : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@end
