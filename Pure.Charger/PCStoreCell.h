//
//  PCStoreCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCStoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *getItButton;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoButton;


@end
