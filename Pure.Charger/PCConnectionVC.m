//
//  PCConnectionVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCConnectionVC.h"
#import "PCMyPointsVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCDeviceListView.h"

@interface PCConnectionVC () {
    PCDeviceListView *deviceView;
}

@end

@implementation PCConnectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // deck controller
    self.deckController.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    [self makeBackButton];
    [self customizeNavigationBar];
    [self showDevicesList];
}

- (void)makeBackButton {
    
    UIImage *backButtonImage = [[UIImage imageNamed:@"NavigationBarButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(tapBack:)];
    [item setTitle:@"Menu"];
//    [item setTintColor:kNavigationBarTintColor];
    
//    [self.navigationItem setLeftBarButtonItem:item];
    [self.navigationItem setHidesBackButton:YES];
}


- (void)showDevicesList {
    double delayInSeconds = 2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self makeDeviceView];
        [self.view addSubview:self.blackView];
        [self.view addSubview:deviceView];
    });
}

- (void)makeDeviceView {
    if (deviceView) {
        return;
    }
    
    deviceView = [PCDeviceListView loadDeviceListView];
    CGRect frame = CGRectMake([self screenWidth]/2 - MEDIUMPOPUPWIDTH/2, MEDIUMPOPUPHEIGHT/2, MEDIUMPOPUPWIDTH, MEDIUMPOPUPHEIGHT);
    deviceView.frame = frame;
//    deviceView.backgroundColor = [UIColor whiteColor];
    deviceView.layer.borderWidth = 1;
    
    [deviceView.chargerButton addTarget:self action:@selector(tapChargerButton) forControlEvents:UIControlEventTouchUpInside];
    [deviceView.phoneButton addTarget:self action:@selector(tapPhoneButton) forControlEvents:UIControlEventTouchUpInside];
    [deviceView.dismissButton addTarget:self action:@selector(tapDismissButton) forControlEvents:UIControlEventTouchUpInside];
    [deviceView.connectButton addTarget:self action:@selector(tapConnectButton) forControlEvents:UIControlEventTouchUpInside];
    
    [deviceView.chargerButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [deviceView.chargerButton setImage:[UIImage imageNamed:@"radioUnselected"] forState:UIControlStateNormal];
    [deviceView.phoneButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [deviceView.phoneButton setImage:[UIImage imageNamed:@"radioUnselected"] forState:UIControlStateNormal];
    
    [deviceView.chargerButton setSelected:YES];
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

#pragma mark - Actions

- (IBAction)sendEmail:(id)sender {
    [self showEmailPageInViewController:self];
}

- (void)tapBack:(id)sender {
//    self.deckController.leftSize = self.screenWidth - SIDEBARWIDTH;
    self.deckController.enabled = YES;
    [self.deckController toggleLeftViewAnimated:YES];
}

- (void)tapChargerButton {
    [deviceView.phoneButton setSelected:NO];
    [deviceView.chargerButton setSelected:YES];
    
    deviceView.chargerLabel.textColor = kLightGreenColor;
    deviceView.phoneLabel.textColor = [UIColor whiteColor];
}

- (void)tapPhoneButton {
    [deviceView.phoneButton setSelected:YES];
    [deviceView.chargerButton setSelected:NO];
    
    deviceView.chargerLabel.textColor = [UIColor whiteColor];
    deviceView.phoneLabel.textColor = kLightGreenColor;
}

- (void)tapDismissButton {
//    [deviceView removeFromSuperview];
    [self tapConnectButton];
}

- (void)tapConnectButton {
    [deviceView removeFromSuperview];
    [self.blackView removeFromSuperview];
    
    self.label.text = TEXTCONNECTED;
    
    double delayInSeconds = 1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        PCMyPointsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCMyPointsVC" ];
        [self.navigationController pushViewController:vc animated:YES];
    });
}

@end
