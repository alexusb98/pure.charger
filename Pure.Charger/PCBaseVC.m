//
//  PCBaseVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "AppDelegate.h"
#import "PCAchievementView.h"

@interface PCBaseVC () {
    MFMailComposeViewController *mailer;
}

@end

@implementation PCBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setup];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.deckController = appDelegate.deckController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)screenWidth {
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

- (CGFloat)screenHeight {
    return CGRectGetHeight([[UIScreen mainScreen] bounds]);
}

#pragma mark - Helpers
- (void)setup {

}

- (void)customizeNavigationBar {
    //navigationBar
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setBarTintColor:kVeryDarkGreyColor];
    NSDictionary *attr = [[NSDictionary alloc] initWithObjects:@[[UIColor whiteColor], k20Font] forKeys:@[NSForegroundColorAttributeName, NSFontAttributeName]];
    [self.navigationController.navigationBar setTitleTextAttributes:attr];
}

#pragma mark NSUserDefaults
- (void)saveStringDefault:(NSString*)string forKey:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setValue:string forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)loadStringDefaultForKey:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

#pragma mark Profile
- (void)saveProfileName:(NSString*)name {
    [self saveStringDefault:name forKey:USERNAMEKEY];
}

- (NSString*)loadProfileName {
    NSString *name = [self loadStringDefaultForKey:USERNAMEKEY];
    return name;
}

- (void)saveProfilePassword:(NSString*)password {
    [self saveStringDefault:password forKey:USERPASSWORDKEY];
}

- (NSString*)loadProfilePassword {
    NSString *password = [self loadStringDefaultForKey:USERPASSWORDKEY];
    return password;
}

- (void)saveProfileEmail:(NSString *)email {
    [self saveStringDefault:email forKey:USEREMAILKEY];
}

- (NSString *)loadProfileEmail {
    NSString *email = [self loadStringDefaultForKey:USEREMAILKEY];
    return email;
}

- (void)saveProfileImagePath:(NSString*)path {
    [self saveStringDefault:path forKey:USERPICTUREPATHKEY];
}

- (NSString*)loadProfileImagePath {
    NSString *path = [self loadStringDefaultForKey:USERPICTUREPATHKEY];
    return path;
}

#pragma mark Alert
- (void)showAlertInViewController:(UIViewController *)viewController withTitle:(NSString *)title message:(NSString *)message {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [viewController presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark Email
- (void)showEmailPageInViewController:(UIViewController *)viewController {
    if ([MFMailComposeViewController canSendMail]) {
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
//        [mailer setSubject:self.subjectTextField.text];
//        NSArray *toRecipients = [NSArray arrayWithObjects:self.contactToEmail.email, nil];
//        [mailer setToRecipients:toRecipients];
//        NSString *emailBody = self.messageTextView.text;
//        [mailer setMessageBody:emailBody isHTML:NO];
        [self presentViewController:mailer animated:YES completion:NULL];
    }
    else {
        [self showAlertInViewController:viewController withTitle:@"Failure" message:@"Your device doesn't support the composer sheet"];
    }
}

#pragma mark - MailComposeController delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [mailer dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Button
- (UIBarButtonItem*)makeMenuButton {
    UIImage *backButtonImage = [[UIImage imageNamed:@"MenuButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(tapBack:)];
    return item;
}

#pragma mark AchievementView
- (void)initBlackView {
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], [self screenHeight])];
    self.blackView.backgroundColor = [UIColor blackColor];
    self.blackView.alpha = 0.7;
}

- (void)initAchievementViewFrames {
    self.achievementViewHiddenFrame = CGRectMake([self screenWidth], [self screenHeight]/2 -SQUAREPOPUPHEIGHT/2 - 64, SQUAREPOPUPWIDTH, SQUAREPOPUPHEIGHT);
    self.achievementViewShownFrame = CGRectMake([self screenWidth]/2 - SQUAREPOPUPWIDTH/2, [self screenHeight]/2 -SQUAREPOPUPHEIGHT/2 - 64, SQUAREPOPUPWIDTH, SQUAREPOPUPHEIGHT);
}

- (PCAchievementView*)makeAchievementViewWithSVGFile:(NSString *)fileName andTitle:(NSString *)title{
    
    PCAchievementView *achievementView = [PCAchievementView loadAchievementView];
    achievementView.frame = self.achievementViewHiddenFrame;
    
    achievementView.layer.borderColor = [UIColor whiteColor].CGColor;
    achievementView.layer.borderWidth = 1;
    
    achievementView.titleLabel.text = title;
    [PCAchievementView showSVGAnimationFromFile:fileName inView:achievementView
     ];
    
    [achievementView.dismissButton addTarget:self action:@selector(tapAchievementViewDismissButton) forControlEvents:UIControlEventTouchUpInside];
    [achievementView.shareButton addTarget:self action:@selector(tapAchievementViewShareButton) forControlEvents:UIControlEventTouchUpInside];
    
    return achievementView;
}

- (void)showAchievementViewWithSVGFile:(NSString *)fileName andTitle:(NSString *)title delay:(NSInteger)delay animatedLabel:(BOOL)animatedLabel{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        self.achievementView = nil;
        self.achievementView = [self makeAchievementViewWithSVGFile:fileName andTitle:title];
        
        
        [self.view addSubview:self.blackView];
        [self.view addSubview:self.achievementView];
        
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             self.achievementView.frame = self.achievementViewShownFrame;
                         }
                         completion:^(BOOL finished){
                             if (animatedLabel) {
                                 [self animatePoints];
                             }
                         }];
    });
}

- (void)animatePoints {
    self.achievementView.pointsLabel.transform = CGAffineTransformScale(self.achievementView.pointsLabel.transform, 20, 20);
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         self.achievementView.pointsLabel.transform = CGAffineTransformScale(self.achievementView.pointsLabel.transform, 0.05, 0.05);
                     }
                     completion:^(BOOL finished){
                     }];

}

#pragma mark - Actions
- (void)tapBack:(id)sender {
    self.deckController.enabled = YES;
    [self.deckController toggleLeftViewAnimated:YES];
}

- (void)tapAchievementViewDismissButton {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.achievementView.frame = self.achievementViewHiddenFrame;
                     }
                     completion:^(BOOL finished){
                         [self.blackView removeFromSuperview];
                         [self.achievementView removeFromSuperview];
                     }];
}

- (void)tapAchievementViewShareButton {
    [self tapAchievementViewDismissButton];
}
@end
