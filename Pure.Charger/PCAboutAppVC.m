//
//  PCAboutAppVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCAboutAppVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCAboutAppSection.h"
#import "PCAboutAppCell.h"

#define kNumber 10

@interface PCAboutAppVC () {
    NSArray *dataSectionSource;
    NSArray *dataRowSource0;
    NSArray *dataRowSource1;
    NSArray *dataRowSource2;
    NSMutableArray *dataSource0;
    NSMutableArray *dataSource1;
    NSMutableArray *dataSource2;
}

@end

@implementation PCAboutAppVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.deckController.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    
    self.navigationItem.title = TITLEBARABOUTAPP;
    dataSectionSource = @[@"Main information", @"App version", @"Contacts"];
    dataRowSource0 = @[@"Demo version"];
    dataRowSource1 = @[@"0.01"];
    dataRowSource2 = @[@"Email"];

    dataSource0 = [[NSMutableArray alloc] init];
    dataSource1 = [[NSMutableArray alloc] init];
    dataSource2 = [[NSMutableArray alloc] init];

    [self makeBackButton];
}

- (void)makeBackButton {
    UIBarButtonItem *item = [self makeMenuButton];
    [self.navigationItem setLeftBarButtonItem:item];
}


- (void)placeImageNamed:(NSString*)imageName toSection:(NSInteger)section {
    UIView *view = [self.tableView viewWithTag:section + kNumber];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[PCAboutAppSection class]]) {
            PCAboutAppSection *cell = (PCAboutAppSection*)subView;
            [UIView animateWithDuration:0.5 animations:^{
                cell.cellImage.image = [UIImage imageNamed:imageName];
            }];
            
            break;
        }
    }
}

- (void)selectButton:(UIButton*) button inSection:(NSInteger)section {
    
    button.selected = YES;
    [self.tableView beginUpdates];
    switch (section) {
        case 0:
            [dataSource0 addObject:dataRowSource0[0]];
            break;
        case 1:
            [dataSource1 addObject:dataRowSource1[0]];
            break;
        case 2:
            [dataSource2 addObject:dataRowSource2[0]];
            break;
        default:
            break;
    }
    
    [self placeImageNamed:@"imageExpandUp" toSection:section];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

- (void)deselectButton:(UIButton*) button inSection:(NSInteger)section {
    button.selected = NO;
    [self.tableView beginUpdates];
    switch (section) {
        case 0:
            [dataSource0 removeAllObjects];
            break;
        case 1:
            [dataSource1 removeAllObjects];
            break;
        case 2:
            [dataSource2 removeAllObjects];
            break;
        default:
            break;
    }
    [self placeImageNamed:@"imageExpandDown" toSection:section];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}

#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PCAboutAppSection *cell = (PCAboutAppSection *)[tableView dequeueReusableCellWithIdentifier:@"PCAboutAppSection"];
    cell.label.text = dataSectionSource[section];
    
    CGRect frame = CGRectMake(CGRectGetMinX(cell.frame), CGRectGetMinY(cell.frame), self.screenWidth, CGRectGetHeight(cell.frame));
    cell.frame = frame;
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    button.tag = section;
    [button addTarget: self action: @selector(sectionTap:) forControlEvents: UIControlEventTouchUpInside];
    [cell addSubview: button];
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.tag = section + kNumber;
    [view addSubview:cell];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = SMALLCELLHEIGHT;
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = SMALLCELLHEIGHT;
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number;
    switch (section) {
        case 0:
            number = dataSource0.count;
            break;
        case 1:
            number = dataSource1.count;
            break;
        case 2:
            number = dataSource2.count;
            break;
        default:
            break;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    PCAboutAppCell*cell = (PCAboutAppCell *)[tableView dequeueReusableCellWithIdentifier:@"PCAboutAppCell"];

    NSString *text;
    switch (indexPath.section) {
        case 0:
            text = dataSource0[0];
            break;
        case 1:
            text = dataSource1[0];
            break;
        case 2:
            text = dataSource2[0];
            break;
        default:
            break;
    }
    cell.label.text = text;
    
//    UIView *view = [[UIView alloc] initWithFrame:cell.frame];
//    view.backgroundColor = kAlmostBlackColor;
//    cell.selectedBackgroundView = view;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

#pragma mark Actions
-(void)sectionTap:(UIButton*)sender {
    if (!sender.selected) {
        [self selectButton:sender inSection:sender.tag];
    } else {
        [self deselectButton:sender inSection:sender.tag];
    }
}

@end
