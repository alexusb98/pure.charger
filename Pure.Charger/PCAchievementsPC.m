//
//  PCAchievementsPC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 24/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCAchievementsPC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCAchievementRowView.h"

//#import "PocketSVG.h"
//#import "TGDrawSvgPathView.h"
//#import <SVGKit/SVGKit.h>
#import <SVGPathSerializing/SVGPathSerializing.h>


#define kGap 10
#define kDistance 5
#define kRowHeight 162

@interface PCAchievementsPC () {
    NSArray *imageSource;
    NSArray *dataSource;
    NSArray *svgSource;
    
    CGFloat yPosition;
}

@end

@implementation PCAchievementsPC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.deckController.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    yPosition = 0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self fillScreen];
    
    //scrollView
    CGFloat scrollViewHeight = CGRectGetHeight(self.scrollView.frame);
    CGFloat newHeight = scrollViewHeight;
    if (scrollViewHeight < yPosition) {
        newHeight = yPosition;
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, newHeight);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    [self makeBackButton];
    
    self.navigationItem.title = TITLEBARACHIEVEMENTS;
    
    dataSource = @[@"RECHARGER STAR", @"GEO CHECKER", @"ENERGY EXPLORER", @"SAFETY COMBO X10", @"COFEE LOVER", @"SHOPPING MANIA", @"EARLY BIRD", @"DINNER KING", @"HANDSHAKE NINJA", @"AIRPORT X3 BONUS", @"NIGHT ROAMER", @"UNDER CONTROL", @"", @"", @""];
    svgSource = @[@"01_recharger_star", @"geo_checker_02", @"03_energy_explorer", @"04_safety_combo_x10", @"05_coffee_lover", @"shopping_maniac_06", @"07_early_bird", @"08_dinner_king", @"09_handshake_ninja", @"10_airport_x3_bonus", @"11_night_roamer", @"12_under_control", @"13_noname_achievement", @"13_noname_achievement", @"13_noname_achievement"];
    imageSource = @[@"01_recharger_star", @"02_geo_checker", @"03_energy_explorer", @"04_safety_combo", @"05_coffee_lover", @"06_shopping_mania", @"07_early_bird", @"08_dinner_king", @"09_handshake_ninja", @"10_airport_x3_bonus", @"11_night_roame", @"12_under_control", @"13_noname_achievement", @"13_noname_achievement", @"13_noname_achievement"];
    
    //AchievementView
    [self initAchievementViewFrames];
    [self initBlackView];
    
}

- (void)makeBackButton {
    UIBarButtonItem *item = [self makeMenuButton];
    [self.navigationItem setLeftBarButtonItem:item];
}

- (void)fillScreen {
    for (int i = 0; i <= 4; i++) {
        [self showRowAtIndex:i];
    }
}

- (void)showRowAtIndex:(NSInteger)index {
    PCAchievementRowView *rowView = [self makeRowAtIndex:index];
    rowView.alpha = 0;
    [self.scrollView addSubview:rowView];
    [self showRowAnimated:rowView withDelay:0.2 * index];
}

- (void)showRowAnimated:(PCAchievementRowView *)row withDelay:(CGFloat)delay {
    CGRect newFrame = CGRectMake(CGRectGetMinX(row.frame), CGRectGetMinY(row.frame) - kGap, CGRectGetWidth(row.frame), CGRectGetHeight(row.frame));
    [UIView animateWithDuration:0.5
                          delay:delay
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         row.alpha = 100;
                         row.frame = newFrame;
                     }
                     completion:nil];
}

- (PCAchievementRowView *)makeRowAtIndex:(NSInteger)index {
    PCAchievementRowView *rowView = [self makeRowView];
    
//    [rowView.button1 setBackgroundImage:[UIImage imageNamed:imageSource[0 + index * 3]] forState:UIControlStateNormal];
//    [rowView.button2 setBackgroundImage:[UIImage imageNamed:imageSource[1 + index * 3]] forState:UIControlStateNormal];
//    [rowView.button3 setBackgroundImage:[UIImage imageNamed:imageSource[2 + index * 3]] forState:UIControlStateNormal];
    [rowView.button1 setImage:[UIImage imageNamed:imageSource[0 + index * 3]] forState:UIControlStateNormal];
    [rowView.button2 setImage:[UIImage imageNamed:imageSource[1 + index * 3]] forState:UIControlStateNormal];
    [rowView.button3 setImage:[UIImage imageNamed:imageSource[2 + index * 3]] forState:UIControlStateNormal];
    
    if (index < 3) {
        rowView.tintColor = kLightGreenColor;
        [rowView.button1 addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
        [rowView.button2 addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
        [rowView.button3 addTarget:self action:@selector(tapButton:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        rowView.tintColor = [UIColor grayColor];
    }
//    DLog(@"index: %i", index);
    
    rowView.button1.tag = 0 + index * 3;
    rowView.button2.tag = 1 + index * 3;
    rowView.button3.tag = 2 + index * 3;
    
    rowView.title1.text = dataSource[0 + index * 3];
    rowView.title2.text = dataSource[1 + index * 3];
    rowView.title3.text = dataSource[2 + index * 3];
    
    return rowView;
}

- (PCAchievementRowView *)makeRowView {
    yPosition = yPosition + kDistance + kGap;
    PCAchievementRowView *rowView = [PCAchievementRowView loadAchievementRowView];
    CGFloat width = [self screenWidth] - 2*kDistance;
    CGRect frame = CGRectMake(([self screenWidth] - width)/2, yPosition, width, kRowHeight);
    rowView.frame = frame;
    
    yPosition = yPosition + kRowHeight;
    
    return rowView;
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

#pragma mark - Actions
- (void)tapButton:(UIButton*)button {
    [self showAchievementViewWithSVGFile:svgSource[button.tag] andTitle:dataSource[button.tag] delay:0.2 animatedLabel:NO];
}

@end
