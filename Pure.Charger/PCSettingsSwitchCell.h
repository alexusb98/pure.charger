//
//  PCSettingsSwitchCell.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 28/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCSettingsSwitchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;

@end
