//
//  PCLogonVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCLogonVC.h"
#import "PCMyPointsVC.h"
#import "PCCreateAccount.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCConnectionVC.h"
#import "PCConnectView.h"


@interface PCLogonVC () {
    BOOL keyboardVisible;
    CGFloat keyboardHeight;
    NSString *userEmail;
    NSString *userPassword;
    CALayer *bottomBorder1;
    CALayer *bottomBorder2;
    PCConnectView *connectView;
}

@end

@implementation PCLogonVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self initBlackView];
    
//    self.emailTextField.text = @"ios_1@ukr.net";
//    self.passwordTextField.text = @"123456";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Keyboard show/hide notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardDidHide:)
                                                 name: UIKeyboardDidHideNotification
                                               object:nil];
    
    //navigationBar
    [self.navigationController.navigationBar setBarTintColor:kExtremelyDarkGreyColor];
    
//    [self.emailTextField becomeFirstResponder];

//    [self addConstraints];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self customizeNavigationBar];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - keyboard Notifications
- (void)keyboardDidShow:(NSNotification *)notification {
    // If keyboard is visible, return
    if (keyboardVisible) {
        return;
    }
    
    //Keyboard height
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrame = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrame CGRectValue];
    keyboardHeight = CGRectGetHeight(keyboardFrameRect);
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame) + keyboardHeight);
    
    // Keyboard is now visible
    keyboardVisible = YES;
    
}

- (void)keyboardDidHide:(NSNotification *)notification {
    // Is the keyboard already shown
    if (!keyboardVisible) {
        return;
    }
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame));
                     }
                     completion:^(BOOL result){
                         
                     }];
//    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, CGRectGetHeight(self.scrollView.frame));
    
    // Keyboard is no longer visible
    keyboardVisible = NO;
}

#pragma mark - textField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if (textField == self.passwordTextField){
        [self hideKeyboard];
//        [bottomBorder removeFromSuperlayer];
        [self login:self];
    }
    
    return YES;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    [bottomBorder removeFromSuperlayer];
//    [textField.layer addSublayer:bottomBorder];
//    return YES;
//}


#pragma mark - Helpers

- (void)setup {
    
    [self addConstraints];

    //textFields
    [self makeBottomBorders];
    [self.emailTextField.layer addSublayer:bottomBorder1];
    [self.passwordTextField.layer addSublayer:bottomBorder2];
    
    NSAttributedString *emailHolder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{ NSForegroundColorAttributeName : kLightGreenColor }];
    self.emailTextField.attributedPlaceholder = emailHolder;
    
    NSAttributedString *passwordHolder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{ NSForegroundColorAttributeName : kLightGreenColor }];
    self.passwordTextField.attributedPlaceholder = passwordHolder;
    //title
    NSString *string = self.titleLabel.text;
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttribute:NSFontAttributeName
                      value:k29BoldFont
                      range:NSMakeRange(5, 7)];
    self.titleLabel.text = @"";
    self.titleLabel.attributedText = attString;
}

- (UIView *)findFirstResonder:(UIView*)root{
    if ([root isFirstResponder]) {
        return root;
    }
    
    for (UIView *subView in root.subviews) {
        UIView *firstResponder = [self findFirstResonder:subView];
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    return nil;
}

- (void)hideKeyboard {
    [[self findFirstResonder:self.scrollView] endEditing:YES];
}

- (void)showConnectionScreen {
    PCConnectionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCConnectionVC" ];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)makeBottomBorders {
    bottomBorder1 = [CALayer layer];
    bottomBorder2 = [CALayer layer];
    CGRect frame = CGRectMake(CGRectGetMinX(self.emailTextField.frame),
                                    CGRectGetHeight(self.emailTextField.frame) - TEXTFIELDBORDER,
                                    [self screenWidth], TEXTFIELDBORDER);
    bottomBorder1.frame = frame;
    bottomBorder1.backgroundColor = kLightGreyColor.CGColor;
    
    bottomBorder2.frame = frame;
    bottomBorder2.backgroundColor = kLightGreyColor.CGColor;
    
}

- (void)makeConnectView {
    if (connectView) {
        return;
    }
    
    connectView = [PCConnectView loadConnectView];
    CGRect frame = CGRectMake([self screenWidth]/2 - SMALLPOPUPWIDTH/2, SMALLPOPUPHEIGHT/2, SMALLPOPUPWIDTH, SMALLPOPUPHEIGHT);
    connectView.frame = frame;
    connectView.layer.borderColor = [UIColor whiteColor].CGColor;
    connectView.layer.borderWidth = 1;
    
    [connectView.laterButton addTarget:self action:@selector(tapLaterButton) forControlEvents:UIControlEventTouchUpInside];
    [connectView.connectButton addTarget:self action:@selector(tapConnectButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addConstraints {

    self.createAccountButton.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat distance = [self screenHeight]  - CREATEACCOUNTBUTTONHEIGHT - 64 - 1;
    
    [self.scrollView addSubview:self.horizontalView];
    [self.scrollView addSubview:self.createAccountButton];
  
    NSArray *verticalConstraints =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-distance-[view1]-0-[view2]"
                                                                           options:0
                                                                           metrics:@{
                                                                                     @"distance" : @(distance)
                                                                                     }
                                                                             views:@{
                                                                                     @"view1" : self.horizontalView,
                                                                                     @"view2" : self.createAccountButton
                                                                                     }];

    [self.scrollView addConstraints:verticalConstraints];
    
}

#pragma mark - Actions

- (IBAction)login:(id)sender {
    userEmail = nil;
    userPassword = nil;
/*
    userEmail = [self loadProfileEmail];
    if (![userEmail isEqualToString:self.emailTextField.text]) {
        [self showAlertInViewController:self withTitle:TITLEALERTWRONGNAME message:TEXTALERTWRONGNAME];
        return;
    }
    
    userPassword = [self loadProfilePassword];
    if (![userPassword isEqualToString:self.passwordTextField.text]) {
        [self showAlertInViewController:self withTitle:TITLEALERTWRONGPASSWORD message:TEXTALERTWRONGPASSWORD];
        return;
    }
*/
    [self hideKeyboard];
    [self makeConnectView];
    [self.view addSubview:self.blackView];
    [self.view addSubview:connectView];
    
}

- (IBAction)loginFB:(id)sender {
//    [self showConnectionScreen];
    [self hideKeyboard];

}

- (IBAction)createAccount:(id)sender {
    [self hideKeyboard];
    PCCreateAccount *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCCreateAccount" ];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapLaterButton {
    [self.blackView removeFromSuperview];
    [connectView removeFromSuperview];
    
    PCMyPointsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PCMyPointsVC" ];
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)tapConnectButton {
    [self.blackView removeFromSuperview];
    [connectView removeFromSuperview];
    [self showConnectionScreen];
}




@end