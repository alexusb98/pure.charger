//
//  PCAchievementsPC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 24/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"

@interface PCAchievementsPC : PCBaseVC <IIViewDeckControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
