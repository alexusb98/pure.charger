//
//  PCMenuVC.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 14/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCBaseVC.h"

@interface PCMenuVC : PCBaseVC <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end
