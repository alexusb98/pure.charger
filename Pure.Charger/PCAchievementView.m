//
//  PCAchievementView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 21/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCAchievementView.h"
#import "PCConstants.h"
#import <SVGPathSerializing/SVGPathSerializing.h>

#define kLineWidth 2

@interface PCAchievementView ()


@end

@implementation PCAchievementView


+ (PCAchievementView *) loadAchievementView {
    PCAchievementView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCAchievementView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];
//        [self showCircleAnimationInView:view];
    }
    
    return view;
}


+ (void)showCircleAnimationInView:(UIView*)view {
    
/*
    // Set up the shape of the circle
    NSInteger radius = 35;
    CAShapeLayer *circle = [CAShapeLayer layer];
    //     Make a circular shape
    CGPoint center = CGPointMake(CGRectGetWidth(view.bounds)/2, 110);
    
    circle.path = [UIBezierPath bezierPathWithArcCenter:center
                                                 radius:radius
                                             startAngle:-M_PI/4
                                               endAngle:0
                                              clockwise:NO].CGPath;
    
    
    // Configure the apperance of the circle
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = kLightGreenColor.CGColor;
    circle.lineWidth = kLineWidth;
    
    circle.strokeStart = 0;
    circle.strokeEnd = 1;
    
    // Add to parent layer
    [view.layer addSublayer:circle];
    
    
    /////////// Lightning
    CAShapeLayer *figure = [CAShapeLayer layer];
    UIBezierPath *figurePath = [UIBezierPath bezierPath];
    CGPoint figureTop = CGPointMake(center.x + 3, center.y - 18);
    [figurePath moveToPoint:figureTop];
    [figurePath addLineToPoint:CGPointMake(figureTop.x , figureTop.y + 15)];
    [figurePath addLineToPoint:CGPointMake(figureTop.x + 6, figureTop.y + 15)];
    [figurePath addLineToPoint:CGPointMake(figureTop.x - 6, figureTop.y + 37)];
    [figurePath addLineToPoint:CGPointMake(figureTop.x - 6, figureTop.y + 21)];
    [figurePath addLineToPoint:CGPointMake(figureTop.x - 12, figureTop.y + 21)];
    [figurePath closePath];
    figure.path = figurePath.CGPath;
    
    // Configure the appearance
    figure.fillColor = [UIColor clearColor].CGColor;
    figure.strokeColor = kLightGreenColor.CGColor;
    figure.lineWidth = kLineWidth;
    
    figure.strokeStart = 0;
    figure.strokeEnd = 1;
    // Add to parent layer
    [view.layer addSublayer:figure];
    
    /////////// Arrow
    CAShapeLayer *arrow = [CAShapeLayer layer];
    UIBezierPath *arrowPath = [UIBezierPath bezierPath];
    CGPoint arrowTop = CGPointMake(center.x + radius * sin(M_PI/4), center.y - radius * sin(M_PI/4));
//    [arrowPath moveToPoint:arrowTop];
//    [arrowPath addLineToPoint:CGPointMake(arrowTop.x , arrowTop.y - 8)];
//    [arrowPath moveToPoint:arrowTop];
//    [arrowPath addLineToPoint:CGPointMake(arrowTop.x - 8, arrowTop.y)];
    
    [arrowPath moveToPoint:CGPointMake(arrowTop.x, arrowTop.y + 1)];
    [arrowPath addLineToPoint:CGPointMake(arrowTop.x , arrowTop.y - 8)];
    [arrowPath moveToPoint:CGPointMake(arrowTop.x + 1, arrowTop.y)];
    [arrowPath addLineToPoint:CGPointMake(arrowTop.x - 8, arrowTop.y)];
    
    arrow.path = arrowPath.CGPath;
    
    // Configure the appearance
    arrow.fillColor = [UIColor clearColor].CGColor;
    arrow.strokeColor = kLightGreenColor.CGColor;
    arrow.lineWidth = kLineWidth;
    
    arrow.strokeStart = 0;
    arrow.strokeEnd = 1;
    // Add to parent layer
    [view.layer addSublayer:arrow];
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 2.0; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add the animation to the circle
    [circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    [figure addAnimation:drawAnimation forKey:@"drawFigureAnimation"];
    [arrow addAnimation:drawAnimation forKey:@"arrowFigureAnimation"];
 */
}

+ (void)showSVGAnimationFromFile:(NSString *)fileName inView:(UIView *)view{
    CABasicAnimation *drawAnimation = [self prepareAnimation];
    CGPoint position = CGPointMake(CGRectGetWidth(view.bounds)/2 - 36, 70);
    NSString *svgPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"svg"];
    NSString *svgString = [NSString stringWithContentsOfFile:svgPath usedEncoding:NULL error:NULL];
    
    for(id path in CGPathsFromSVGString(svgString, NULL)) {
        // Create a layer for each path
        CAShapeLayer *layer = [CAShapeLayer layer];
        layer.path = (__bridge CGPathRef)path;
        
        // Set its display properties
        layer.position = position;
        layer.lineWidth   = 2;
        layer.strokeColor = kLightGreenColor.CGColor;
        layer.fillColor   = [[UIColor blackColor] CGColor];
        
        // Add it to the layer hierarchy
        [view.layer addSublayer:layer];
        // Add the animation
        [layer addAnimation:drawAnimation forKey:[NSString stringWithFormat: @"%@", layer.description]];
    }
}

+ (CABasicAnimation*)prepareAnimation {
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 2.0; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    return drawAnimation;
}


@end
