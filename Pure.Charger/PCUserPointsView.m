//
//  PCUserPointsView.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 12/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCUserPointsView.h"

@interface PCUserPointsView ()

@end

@implementation PCUserPointsView

+ (PCUserPointsView *) loadUserPointsView{
    PCUserPointsView *view = nil;
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"PCUserPointsView" owner:self options:nil];
    if ([nibs count] > 0) {
        view = [nibs objectAtIndex:0];
    }
    
    return view;
}

@end
