//
//  PCUserPointCell.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 16/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCUserPointCell.h"

@implementation PCUserPointCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
