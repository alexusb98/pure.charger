//
//  PCUserHistoryVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 15/02/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCUserHistoryVC.h"
#import "PCSource.h"
#import "PCUserPointCell.h"

@interface PCUserHistoryVC () {
    CLLocationManager  *locationManager;
    CLLocation *userLocation;
    CLLocation *bestEffortAtLocation;
    CLLocationDegrees userLatitude;
    CLLocationDegrees userLongitude;
    CGRect mapViewShownFrame;
    CGRect mapViewCollapsedFrame;
    CGRect containerViewShownFrame;
    CGRect containerViewCollapsedFrame;
    CGRect mapButtonShownFrame;
    CGRect mapButtonCollapsedFrame;
    CGRect containerButtonShownFrame;
    CGRect containerButtonCollapsedFrame;
}

@end

@implementation PCUserHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.deckController.delegate = self;
    [self moveMapToUserLocation:self.mapView];
    [self.containerButton setImage:[UIImage imageNamed:@"buttonDown"] forState:UIControlStateSelected];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self calculateFrames];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Map
- (void)moveMapToUserLocation:(MKMapView*)map {
    //    CLLocation *location = [[CLLocation alloc] initWithLatitude:userLatitude longitude:userLongitude];
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2 * BARDISTANCE, 2 * BARDISTANCE);
    //    [map setRegion:region animated:YES];
    
    //Дворцовую площадь, например, питера там покажите
    MKCoordinateRegion region;
    region.center.latitude = 59.933217 ;
    region.center.longitude = 30.317772;
    
    region.span.latitudeDelta = 0.02;
    region.span.longitudeDelta = 0.05;
    
    [map setRegion:region animated:YES];
    
    userLatitude = region.center.latitude;
    userLongitude = region.center.longitude;
    
    [self placeBarAnnotations];
}



- (void)placeBarAnnotations {
//    WBarMapAnnotation *annotation0 = [[WBarMapAnnotation alloc] initWithLatitude:userLatitude  andLongitude:(userLongitude + 0.008 )];
//    annotation0.title = @"Бар Jack&Jan";
//    WBarMapAnnotation *annotation1 = [[WBarMapAnnotation alloc] initWithLatitude:userLatitude - 0.004 andLongitude:(userLongitude - 0.008 )];
//    annotation1.title = @"Бар Playoff";
//    WBarMapAnnotation *annotation2 = [[WBarMapAnnotation alloc] initWithLatitude:userLatitude   + 0.008 andLongitude:userLongitude];
//    annotation2.title = @"Бар Keg&Ball";
//    [self.mapView addAnnotations:@[annotation0, annotation1, annotation2]];
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    MKAnnotationView *annotationView;
    annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"MapPin"];
    
    if(!annotationView) {
        annotationView=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapPin"];
    }
    
    annotationView.image = [UIImage imageNamed:@"MapPin"];
    annotationView.canShowCallout = YES;
    annotationView.calloutOffset = CGPointMake(-5, 5);
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    MKCoordinateRegion region = mapView.region;
    DLog(@"region.center.latitude: %f", region.center.latitude);
    DLog(@"region.center.longitude: %f", region.center.longitude);
    DLog(@"region.span.latitudeDelta: %f", region.span.latitudeDelta);
    DLog(@"region.span.longitudeDelta: %f", region.span.longitudeDelta);
    //
}


#pragma mark - Location
- (void)initLocationManagement {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] &&
        [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways){
        [locationManager requestWhenInUseAuthorization];
    }
    
    [locationManager startUpdatingLocation];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    DLog(@"Error: %@",error.description);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        DLog(@"Status: %@", @"Authorized");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"locationDataReceived"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
        DLog(@"Status: %@",@"Denied");
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"locationDataReceived"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    userLocation = [locations lastObject];
    if (userLocation.horizontalAccuracy < 0) {
        return;
    }
    
    if (bestEffortAtLocation == nil || bestEffortAtLocation.horizontalAccuracy > userLocation.horizontalAccuracy) {
        // store the location as the "best effort"
        bestEffortAtLocation = userLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        if (userLocation.horizontalAccuracy <= manager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            //
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            
            self.mapView.showsUserLocation = YES;
            DLog(@"userLocation: %@",@"Found");
            userLatitude = userLocation.coordinate.latitude;
            userLongitude = userLocation.coordinate.longitude;
            [self moveMapToUserLocation:self.mapView];
            [self placeBarAnnotations];
            
            [locationManager stopUpdatingLocation];
            
        }
    }
    
}


#pragma mark - Helpers
- (void)setup {

    self.navigationItem.title = TITLEBARUSERHISTORY;
    [self makeBackButton];
}

- (void)calculateFrames {
    mapViewShownFrame = CGRectMake(CGRectGetMinX(self.mapView.frame), CGRectGetMinY(self.mapView.frame), CGRectGetWidth(self.mapView.frame), MINMAPHEIGHT);
    mapViewCollapsedFrame = self.mapView.frame;
    
    containerViewShownFrame = CGRectMake(CGRectGetMinX(self.containerView.frame), MINMAPHEIGHT, CGRectGetWidth(self.containerView.frame), self.screenHeight - MINMAPHEIGHT);
    containerViewCollapsedFrame = self.containerView.frame;
    
    mapButtonShownFrame = CGRectMake(CGRectGetMinX(self.mapButton.frame), CGRectGetMaxY(mapViewShownFrame)- HISTORYMAPBUTTONOFFSET - CGRectGetHeight(self.mapButton.frame), CGRectGetWidth(self.mapButton.frame), CGRectGetHeight(self.mapButton.frame));
    mapButtonCollapsedFrame = self.mapButton.frame;
    
    containerButtonShownFrame = CGRectMake(CGRectGetMinX(self.containerButton.frame), CGRectGetMaxY(mapButtonShownFrame) + HISTORYBUTTONSOFFSET, CGRectGetWidth(self.containerButton.frame), CGRectGetHeight(self.containerButton.frame));
    containerButtonCollapsedFrame = self.containerButton.frame;
}

- (void)makeBackButton {
    UIBarButtonItem *item = [self makeMenuButton];
    [self.navigationItem setLeftBarButtonItem:item];
}

- (void)showContainerView {

    self.containerButton.selected = YES;
    [UIView animateWithDuration:0.5 animations:^{
        self.containerView.frame = containerViewShownFrame;
        self.mapView.frame = mapViewShownFrame;
        self.mapButton.frame = mapButtonShownFrame;
        self.containerButton.frame = containerButtonShownFrame;
        
    } completion:^(BOOL finished){
        
    }];
}

- (void)hideContainerView {
    self.containerButton.selected = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.containerView.frame = containerViewCollapsedFrame;
        self.mapView.frame = mapViewCollapsedFrame;
        self.mapButton.frame = mapButtonCollapsedFrame;
        self.containerButton.frame = containerButtonCollapsedFrame;
    }  completion:^(BOOL finished){
        
    }];
}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

#pragma mark - Actions
- (IBAction)mapButtonTap:(id)sender {
}

- (IBAction)containerButtonTap:(id)sender {
    
    if (!self.containerButton.selected) {
        [self showContainerView];
    } else {
        [self hideContainerView];
    }
}
@end
