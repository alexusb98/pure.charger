//
//  PCHistoryContainerView.h
//  Pure.Charger
//
//  Created by Alexey Yakushev on 07/04/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCHistoryContainerView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *containerImage;
@property (weak, nonatomic) IBOutlet UILabel *containerText;
@property (weak, nonatomic) IBOutlet UIView *borderView;

+ (PCHistoryContainerView *)loadHistoryContainerView;


@end
