//
//  PCMyPointsVC.m
//  Pure.Charger
//
//  Created by Alexey Yakushev on 20/03/16.
//  Copyright © 2016 Alexey Yakushev. All rights reserved.
//

#import "PCMyPointsVC.h"
#import "PCSource.h"
#import "PCConstants.h"
#import "PCAchievementView.h"

@interface PCMyPointsVC () {
    CGRect userImageFrame;
    CGRect userImageFrameNew;
    CGRect userNameLabelFrame;
    CGRect userNameLabelFrameNew;
    CGRect totalLabelFrame;
    CGRect totalLabelFrameNew;
    CGRect progressLabelFrame;
    CGRect progressLabelFrameNew;
    CGRect bonusViewFrame;
    CGRect bonusViewFrameNew;
    CGPoint circleBackgroundPosition;
    CGPoint circleBackgroundPositionNew;
    
    CGFloat coefficient;
    CAShapeLayer *circle;
//    PCAchievementView *achievementView;
//    UIView *blackView;
}

@end

@implementation PCMyPointsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.deckController.delegate = self;
    coefficient = 0.7;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view.
    self.userImage.alpha = 0;
    self.userNameLabel.alpha = 0;
    self.totalLabel.alpha = 0;
    self.progressLabel.alpha = 0;
    self.bonusView.alpha = 0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self screenPrepare];
    [self screenShowPart1];
    [self screenShowPart2];
    [self screenShowPart3];
    
    [self showAchievementViewWithSVGFile:@"01_recharger_star" andTitle:@"RECHARGER STAR" delay:CIRCLEANIMATIONDURATION + 2.5 animatedLabel:YES];
//    self.achievementView.pointsLabel.font = k200Font;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers
- (void)setup {
    [self makeBackButton];
    [self customizeNavigationBar];
    self.navigationItem.title = TITLEBARUSERPOINTS;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    
//    [self.navigationController.navigationBar setBarTintColor:kVeryDarkGreyColor];
//    NSDictionary *attr = [[NSDictionary alloc] initWithObjects:@[[UIColor whiteColor], k20Font] forKeys:@[NSForegroundColorAttributeName, NSFontAttributeName]];
//    [self.navigationController.navigationBar setTitleTextAttributes:attr];
    
    [self makeBonusLabel];
    
    //AchievementView
    [self initAchievementViewFrames];
    [self initBlackView];
}

- (void)screenPrepare {
    userImageFrame = self.userImage.frame;
    userImageFrameNew = CGRectMake(CGRectGetMinX(userImageFrame), CGRectGetMinY(userImageFrame) + ITEMSOFFSET, CGRectGetWidth(userImageFrame), CGRectGetHeight(userImageFrame));
    userNameLabelFrame = self.userNameLabel.frame;
    userNameLabelFrameNew = CGRectMake(CGRectGetMinX(userNameLabelFrame), CGRectGetMinY(userNameLabelFrame) + ITEMSOFFSET, CGRectGetWidth(userNameLabelFrame), CGRectGetHeight(userNameLabelFrame));
    totalLabelFrame = self.totalLabel.frame;
    totalLabelFrameNew = CGRectMake(CGRectGetMinX(totalLabelFrame), CGRectGetMinY(totalLabelFrame) + ITEMSOFFSET, CGRectGetWidth(totalLabelFrame), CGRectGetHeight(totalLabelFrame));
    progressLabelFrame = self.progressLabel.frame;
    progressLabelFrameNew = CGRectMake(CGRectGetMinX(progressLabelFrame), CGRectGetMinY(progressLabelFrame) + ITEMSOFFSET, CGRectGetWidth(progressLabelFrame), CGRectGetHeight(progressLabelFrame));
    bonusViewFrame  = self.bonusView.frame;
    bonusViewFrameNew = CGRectMake(CGRectGetMinX(bonusViewFrame), CGRectGetMinY(bonusViewFrame) + ITEMSOFFSET, CGRectGetWidth(bonusViewFrame), CGRectGetHeight(bonusViewFrame));
    circleBackgroundPosition = CGPointMake(CGRectGetMidX(progressLabelFrame) - CIRCLERADIUS,
                                           CGRectGetMidY(progressLabelFrame) - CIRCLERADIUS);
    circleBackgroundPositionNew = CGPointMake(CGRectGetMidX(progressLabelFrame) - CIRCLERADIUS,
                                              CGRectGetMidY(progressLabelFrame) - CIRCLERADIUS + ITEMSOFFSET);
   
    self.userImage.frame = userImageFrameNew;
    self.userNameLabel.frame = userNameLabelFrameNew;
    self.totalLabel.frame = totalLabelFrameNew;
    self.progressLabel.frame = progressLabelFrameNew;
    self.bonusView.frame = bonusViewFrameNew;
    
    
}

- (void)screenShowPart1 {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.userImage.alpha = 100;
                         self.userNameLabel.alpha = 100;
                         self.userImage.frame = userImageFrame;
                         self.userNameLabel.frame = userNameLabelFrame;
                     }
                     completion:nil];
}

- (void)screenShowPart2 {
    
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        self.totalLabel.alpha = 100;
                        self.progressLabel.alpha = 100;
                        
                        self.totalLabel.frame = totalLabelFrame;
                        self.progressLabel.frame = progressLabelFrame;
                         
                        [self drawCircleBackgroundAnimated];
                        }
                     completion:^(BOOL finished){

                     }];

}

- (void)screenShowPart3 {
    
    [UIView animateWithDuration:0.5
                          delay:1.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{

                         self.bonusView.alpha = 100;
                         self.bonusView.frame = bonusViewFrame;
                     }
                     completion:^(BOOL finished){
                         [self calculateAchievements];
                         [self drawCircleProgress];
                     }];
}


- (void)makeBackButton {
    UIBarButtonItem *item = [self makeMenuButton];
    [self.navigationItem setLeftBarButtonItem:item];
}

- (void)drawCircleBackgroundAnimated {
    NSInteger radius = CIRCLERADIUS;
    circle = [CAShapeLayer layer];
    // Make a circular shape
    circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                             cornerRadius:radius].CGPath;
    
    // Center the shape in self.view
    circle.position = circleBackgroundPositionNew;
    
    // Configure the appearance of the circle
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = kDarkGreyColor.CGColor;
    circle.lineWidth = ARCWIDTH;
    
    circle.strokeStart = 0;
    circle.strokeEnd = 1;
    
    // Add to parent layer

    [self.view.layer addSublayer:circle];
    [self animateCircleBackgroundChanges];
    
}

- (void)animateCircleBackgroundChanges {
    
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    drawAnimation.fromValue = @(0);
    drawAnimation.toValue   = @(1);
    drawAnimation.duration = 0.5;        // 1 second
    drawAnimation.autoreverses = NO;    // Back
    drawAnimation.repeatCount = 1;       // Or whatever
    
    [circle addAnimation:drawAnimation forKey:@"opacity"];
    
    CABasicAnimation *drawAnimation2 = [CABasicAnimation animationWithKeyPath:@"position"];
    drawAnimation2.fromValue = [NSValue valueWithCGPoint:circle.position];
    drawAnimation2.toValue   = [NSValue valueWithCGPoint:circleBackgroundPosition];
    drawAnimation2.duration = 0.5;        // 1 second
    drawAnimation2.autoreverses = NO;    // Back
    drawAnimation2.repeatCount = 1;       // Or whatever
    
    [circle addAnimation:drawAnimation2 forKey:@"position"];
    
    circle.frame = CGRectMake(CGRectGetMinX(circle.frame), CGRectGetMinY(circle.frame)-10, CGRectGetWidth(circle.frame), CGRectGetHeight(circle.frame));
}

- (void)drawCircleProgress {
    // Set up the shape of the circle
    NSInteger radius = CIRCLERADIUS;
    CAShapeLayer *circleFilled = [CAShapeLayer layer];
    // Make a circular shape
    
    CGFloat startAngle = (-M_PI/2);
    CGFloat endAngle = startAngle + coefficient * (2*M_PI);
    
    circleFilled.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(radius, radius)
                                                 radius:radius
                                             startAngle:startAngle
                                               endAngle:endAngle
                                              clockwise:YES].CGPath;
    
    // Center the shape in self.view
    circleFilled.position = CGPointMake(CGRectGetMidX(self.progressLabel.frame) - radius,
                                  CGRectGetMidY(self.progressLabel.frame) - radius);
    
    // Configure the appearance of the circle
    circleFilled.fillColor = [UIColor clearColor].CGColor;
    circleFilled.strokeColor = kLightGreenColor.CGColor;
    circleFilled.lineWidth = ARCWIDTH;
    
    circleFilled.strokeStart = 0;
    circleFilled.strokeEnd = 1;
    
    // Add to parent layer
    [self.view.layer addSublayer:circleFilled];
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = CIRCLEANIMATIONDURATION; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = @(0);
    drawAnimation.toValue   = @(1);
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add the animation to the circle
    [circleFilled addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
}

- (void)calculateAchievements {
    self.progressLabel.method = UILabelCountingMethodLinear;
    self.progressLabel.format = @"%d";
    [self.progressLabel countFrom:0 to:coefficient * 3600 withDuration:CIRCLEANIMATIONDURATION];
}

- (void)makeBonusLabel {
    NSString *string = [NSString stringWithFormat:@"%@%@", TEXTBONUSLABEL, @" +300 PTS"];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    [attString addAttribute:NSForegroundColorAttributeName
                      value:kLightGreenColor
                      range:NSMakeRange(TEXTBONUSLABEL.length, string.length - TEXTBONUSLABEL.length)];
    self.bonusLabel.attributedText = attString;
}

#pragma mark - Actions

//- (void)tapBack:(id)sender {
//    //    self.deckController.leftSize = self.screenWidth - SIDEBARWIDTH;
//    self.deckController.enabled = YES;
//    [self.deckController toggleLeftViewAnimated:YES];
//}

#pragma mark - IIViewDeckControllerDelegate
- (void)viewDeckController:(IIViewDeckController *)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated {
    self.deckController.enabled = NO;
}

@end
